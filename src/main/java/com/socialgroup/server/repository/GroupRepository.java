package com.socialgroup.server.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {

    @Override
	public List<Group> findAll();
    @Override
	public Optional<Group> findById(@Param("id") Long groupId);
    public Collection<Group> findByUserGroupsUser(User user);

    public Collection<Group> findAllByIdIn(Collection<Long> group);
}

