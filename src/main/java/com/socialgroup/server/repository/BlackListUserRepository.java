package com.socialgroup.server.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.BlackListUser;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;

@Repository
public interface BlackListUserRepository extends BlackListRepository<BlackListUser>{

	Optional<BlackListUser> findBySourceGroupAndSourceUser(Group group, User user);

	Optional<BlackListUser> findBySourceGroupAndSourceUserAndChoiceUserBlackListUserEquals(Group groupConcerned, User userToRemove, User userConnected);

}
