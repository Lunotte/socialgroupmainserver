package com.socialgroup.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.Feed;

@Repository
public interface FeedRepository extends JpaRepository<Feed, Long> {

}
