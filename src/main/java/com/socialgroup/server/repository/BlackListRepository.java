package com.socialgroup.server.repository;

import java.time.ZonedDateTime;
import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BlackListRepository<T> extends JpaRepository<T, Long>{

	Collection<T> findAllByDateLimitBefore(ZonedDateTime date);

}
