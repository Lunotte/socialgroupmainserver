package com.socialgroup.server.repository;

import java.time.ZonedDateTime;
import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.Notification;
import com.socialgroup.server.entity.User;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {

	Collection<Notification> findAllBySeenAndUpdatedBefore(boolean seen, ZonedDateTime date);

	Page<Notification> findAllBySourceUser(User user, Pageable pageable);
}
