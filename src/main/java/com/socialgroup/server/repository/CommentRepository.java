package com.socialgroup.server.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Override
	public List<Comment> findAll();
    @Override
	public Optional<Comment> findById(@Param("id") Long commentId);

}

