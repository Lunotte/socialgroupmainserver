package com.socialgroup.server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.ChoiceUserBlackList;

@Repository
public interface ChoiceUserBlackListRepository extends CrudRepository<ChoiceUserBlackList, Long>{

}
