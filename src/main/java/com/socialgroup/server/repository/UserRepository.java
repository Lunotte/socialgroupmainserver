package com.socialgroup.server.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public Optional<User> getByUsername(String username);

	@Override
	public List<User> findAll();

	@Override
	Optional<User> findById(Long userId);

    @Query("select u from User u left join fetch u.roles r where u.email=:email")
    public Optional<User> findByEmailAuth(@Param("email") String email);

    public Optional<User> findByEmail(String email);

    /**
     * Users in this group and not blacklisted
     * @param group
     * @return
     */
    public Optional<Collection<User>> findByUserGroupsGroupAndUserGroupsBlackListedEquals(Group group, ChoiceYesNoEnum choiceYesNoEnum);

}
