package com.socialgroup.server.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.Event;
import com.socialgroup.server.entity.Group;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

   /* public List<Event> findAll();
    public Event findById(@Param("id") Long eventId);
    @Query("select e from Event e join e.user u where u =:user")
    public List<Event> findByUser(@Param("user") User user);

    @Query("select e from Event e join e.group g where g =:group")
    public List<Event> findByGroup(@Param("group") Group group);*/
	
	@Query("select e from Event e where e.source.group = :group")
	public Page<Event> findAll(@Param("group") Group group, Pageable pageable);
}

