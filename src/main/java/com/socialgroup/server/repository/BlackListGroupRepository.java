package com.socialgroup.server.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.socialgroup.server.entity.BlackListGroup;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;

@Repository
public interface BlackListGroupRepository extends BlackListRepository<BlackListGroup>{

	Optional<BlackListGroup> findByGroupAndChoiceGroupBlackListUserEquals(Group groupConcerned, User userConnected);

	Optional<BlackListGroup> findByGroup(Group group);
}
