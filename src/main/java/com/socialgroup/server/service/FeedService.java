package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.server.entity.Feed;
import com.socialgroup.server.repository.FeedRepository;

@Service
public class FeedService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FeedService.class);

	private final FeedRepository feedRepository;

	public FeedService(final FeedRepository feedRepository) {
		this.feedRepository = feedRepository;
	}

	public List<Feed> findAll() throws SgException{

		LOGGER.debug("Find all feed");

		try {
			return this.feedRepository.findAll();
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find feeds", e);
    	}
	}
}
