package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.Comment;
import com.socialgroup.server.repository.CommentRepository;

@Service
public class CommentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommentService.class);

    private final CommentRepository commentRepository;

    public CommentService(final CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

//	public Comment findById(final Long commentId) throws SgException {
//
//		Validators.isNull(commentId, "You have to define the commentId");
//		LOGGER.debug("Find the comment {}", commentId);
//
//		try {
//			final Optional<Comment> comment = this.commentRepository.findById(commentId);
//	        if(comment.isPresent()) {
//	    		return comment.get();
//	    	} else {
//	    		throw new SgItemNotFoundException("The comment was not found");
//	    	}
//		} catch (final Exception e) {
//			throw throwException("An error has occurred to find the comment", e);
//		}
//	}

    public Comment save(final Comment comment) throws SgException{

    	Validators.isNull(comment, "You have to define the comment to save");
		LOGGER.debug("Save the comment");

		try {
			return this.commentRepository.save(comment);
		} catch (final Exception e) {
    		throw throwException("An error has occurred to save the comment", e);
    	}
    }

}