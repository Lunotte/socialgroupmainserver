package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgItemNotFoundException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.endpoint.CreatePostJson;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Post;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.Source;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.repository.RequestRepository;

@Service
public class RequestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestService.class);

    private final RequestRepository requestRepository;

    public RequestService(final RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public List<Request> findAllOrderByDate(final Group group) throws SgException {

        Validators.isNull(group, "You have to define group");
		LOGGER.debug("Find all requests order by date {}", group.getId());

		try {
			return this.requestRepository.findBySourceGroupOrderByCreatedDesc(group);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find request %d", group.getId()), e);
    	}
    }

    public List<Request> findByGroupAndSurveyIsFalseOrderByCreatedDesc(final Group group) throws SgException {

        Validators.isNull(group, "You have to define group");
		LOGGER.debug("Find all requests are not survey and order by date {}", group.getId());

		try {
			return this.requestRepository.findBySourceGroupAndSurveyIsFalseOrderByCreatedDesc(group);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find request %d", group.getId()), e);
    	}
    }

    public List<Request> findAll() throws SgException {

		LOGGER.debug("Find all requests");

		try {
			return this.requestRepository.findAll();
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find requests", e);
    	}
    }

    public Request findById(final Long requestId) throws SgException {

        Validators.isNull(requestId, "You have to define request");
		LOGGER.debug("Find request {}", requestId);

		try {
		 final Optional<Request> request = this.requestRepository.findById(requestId);
	        if(request.isPresent()) {
	    		return request.get();
	    	} else {
	    		throw new SgItemNotFoundException(String.format("Request was not found %d", requestId));
	    	}
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find request %d", requestId), e);
    	}
    }

	public List<Request> findByUser(final User user) throws SgException {

		Validators.isNull(user, "You have to define user");
		LOGGER.debug("Find all requests by user {}", user.getId());

		try {
			return this.requestRepository.findBySourceUser(user);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find requests %d", user.getId()), e);
    	}
	}

	public List<Request> findByGroup(final Group group) throws SgException {

		Validators.isNull(group, "You have to define group");
		LOGGER.debug("Find all requests by group {}", group.getId());

		try {
			return this.requestRepository.findBySourceGroup(group);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find requests %d", group.getId()), e);
    	}
	}

	public Request save(final Request request) throws SgException {

		Validators.isNull(request, "You have to define request");
		LOGGER.debug("Save request");

		try {
			return this.requestRepository.save(request);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to save request %d", request.getId()), e);
    	}
	}

	public void delete(final Request request) throws SgException {

		Validators.isNull(request, "You have to define request");
		LOGGER.debug("Delete request {}", request.getId());

		try {
			this.requestRepository.delete(request);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to delete request %d", request.getId()), e);
    	}
	}

	public Request createRequests(final Post createdPost, final CreatePostJson post, final Source source) throws SgException {

		Validators.isNull(createdPost, "You have to define post");
		Validators.isNull(post, "You have to define post");
		Validators.isNull(source, "You have to define post");
		LOGGER.debug("Create request");

		try {
			final Request request = new Request();
			request.setDescription(createdPost.getMessage());
			request.setPost(createdPost);
			request.setSource(source);

			if(post.isSurvey()) {
				request.setSurvey(true);
			} else {
				request.setStart(post.getStart());
				request.setEnd(post.getEnd());
			}

			return request;
		} catch (final Exception e) {
    		throw throwException("An error has occurred to create request", e);
    	}
    }
}