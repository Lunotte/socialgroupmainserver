package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.CommentSurvey;
import com.socialgroup.server.repository.CommentSurveyRepository;

@Service
public class CommentSurveyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommentSurveyService.class);

	private final CommentSurveyRepository commentSurveyRepository;

	public CommentSurveyService(final CommentSurveyRepository commentSurveyRepository) {
		this.commentSurveyRepository = commentSurveyRepository;
	}

	public CommentSurvey save(final CommentSurvey commentSurvey) throws SgException{

    	Validators.isNull(commentSurvey, "You have to define the comment of survey to save");
		LOGGER.debug("Save the comment");

		try {
			return this.commentSurveyRepository.save(commentSurvey);
		} catch (final Exception e) {
    		throw throwException("An error has occurred to save the comment of survey", e);
    	}
    }

	public void remove(final CommentSurvey commentSurvey) throws SgException{

    	Validators.isNull(commentSurvey, "You have to define the comment of survey to delete");
		LOGGER.debug("Delete the survey comment {}", commentSurvey.getId());

		try {
			this.commentSurveyRepository.delete(commentSurvey);
		} catch (final Exception e) {
    		throw throwException("An error has occurred to save the survey comment", e);
    	}
    }
}
