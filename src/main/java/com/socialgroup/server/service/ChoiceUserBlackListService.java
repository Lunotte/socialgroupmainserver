package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.ChoiceUserBlackList;
import com.socialgroup.server.repository.ChoiceUserBlackListRepository;

@Service
public class ChoiceUserBlackListService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChoiceUserBlackListService.class);

	private final ChoiceUserBlackListRepository choiceUserBlackListRepository;

	public ChoiceUserBlackListService(final ChoiceUserBlackListRepository choiceUserBlackListRepository) {
		this.choiceUserBlackListRepository = choiceUserBlackListRepository;
	}

	/**
	 * Save or Update ChoiceUserBlackList
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public ChoiceUserBlackList save(final ChoiceUserBlackList choiceUserBlackList) throws SgException{

		Validators.isNull(choiceUserBlackList, "You have to define choice for blackListed user");
		LOGGER.debug("Save the choice for blackListed user");

		try {
			return this.choiceUserBlackListRepository.save(choiceUserBlackList);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to choice vote for blacklisted user %d", choiceUserBlackList.getBlackList().getId()), e);
    	}
    }

}
