package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.exception.SgRulesException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.BlackListUser;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;
import com.socialgroup.server.repository.BlackListUserRepository;

@Service
public class BlackListUserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BlackListUserService.class);

	private final BlackListUserRepository blackListUserRepository;


	public BlackListUserService(final BlackListUserRepository blackListUserRepository) {
		this.blackListUserRepository = blackListUserRepository;
	}

	public BlackListUser findByGroupAndUser(final Group group, final User user) throws SgException{

		Validators.isNull(user, "You have to define group");
		Validators.isNull(user, "You have to define user");
		LOGGER.debug("Search BlackListUser by user {} and group {}", user.getId(), group.getId());

		try {
			final Optional<BlackListUser> blackListUser = this.blackListUserRepository.findBySourceGroupAndSourceUser(group, user);
			if(!blackListUser.isPresent()) {
				throw new SgInvalidArgumentException("The user has not found.");
			}
			return blackListUser.get();
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find the user %d", user.getId()), e);
    	}
    }

	/**
	 * Save or Update UserGroup
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public BlackListUser save(final BlackListUser blackListUser) throws SgException{

		Validators.isNull(blackListUser, "You have to define blackListed user");
		LOGGER.debug("Save the blackListUser {}", blackListUser.getId());

		try {
			return this.blackListUserRepository.save(blackListUser);

		} catch (final Exception e) {
    		throw throwException("An error has occurred to save the user blacklisted", e);
    	}
    }

	public void checkAlreadyVoted(final User userConnected, final User userToRemove, final Group groupConcerned) throws SgException {

		LOGGER.debug("Check whether user have already vote for this user and group");

		try {
			final Optional<BlackListUser> blackList = this.blackListUserRepository.findBySourceGroupAndSourceUserAndChoiceUserBlackListUserEquals(groupConcerned, userToRemove, userConnected);
			if(blackList.isPresent()) {
				throw new SgRulesException("You can not voted twice");
			}
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find data", e);
    	}
	}

	private Collection<BlackListUser> getAllBlackListTemporary() throws SgException {
		LOGGER.debug("Get all the blackListed users");

		try {
			return this.blackListUserRepository.findAllByDateLimitBefore(ZonedDateTime.now());
		} catch (final Exception e) {
    		throw throwException("An error has occurred to get list of temporary user blacklisted", e);
    	}
	}

	/**
	 * Get all black listed user and verify whether disable of the group
	 * @throws SgException
	 */
	public void checkBlackListAndDisableUser() throws SgException {
		LOGGER.debug("Check blackListed users");
		try {
			final Collection<BlackListUser> tempBlackList = this.getAllBlackListTemporary();
			for (final BlackListUser blackListUser : tempBlackList) {
				if(blackListUser.canBeBlackListed()) {
					final User user = blackListUser.getSource().getUser();
					final Optional<UserGroup> userGroup = user.getUserGroups().stream().filter(
							ug -> ug.getGroup().equals(blackListUser.getSource().getGroup()) && ug.getUser().equals(blackListUser.getSource().getUser())).findFirst();
					if(userGroup.isPresent()) {
						userGroup.get().setBlackListed(ChoiceYesNoEnum.YES);
					}
					this.blackListUserRepository.delete(blackListUser);
				} else {
					this.blackListUserRepository.delete(blackListUser);
				}
			}


		} catch (final Exception e) {
			throw throwException("An error has occurred to check black listed user", e);
		}
	}

}
