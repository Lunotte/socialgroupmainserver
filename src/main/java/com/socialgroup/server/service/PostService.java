package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgItemNotFoundException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Post;
import com.socialgroup.server.repository.PostRepository;

@Service
public class PostService{

	private static final Logger LOGGER = LoggerFactory.getLogger(PostService.class);

    private final PostRepository postRepository;

    public PostService(final PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public List<Post> findAllOrderByDate(final Group group) throws SgException {

        Validators.isNull(group, "You have to define group");
		LOGGER.debug("Find all poste by group {} order by date", group.getId());

		try {
			return this.postRepository.findByGroupOrderByCreatedDesc(group);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find postes by group %d", group.getId()), e);
    	}
    }

    public Page<Post> findAllPageable(final Group group, final Pageable page) throws SgException {

        Validators.isNull(group, "You have to define group");
		LOGGER.debug("Find all poste by group {}", group.getId());

		try {
			return this.postRepository.findByGroupOrderByCreatedDesc(group, page);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find postes by group %d", group.getId()), e);
    	}
    }

	public Post findById(final Long postId) throws SgException {

        Validators.isNull(postId, "You have to define a post");
		LOGGER.debug("Find poste {}", postId);

		try {
			final Optional<Post> post = this.postRepository.findById(postId);
	        if(post.isPresent()) {
	    		return post.get();
	    	} else {
	    		throw new SgItemNotFoundException(String.format("Post was not found %d", postId));
	    	}
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find posts %d", postId), e);
    	}
	}

    public Post save(final Post post) throws SgException{

    	Validators.isNull(post, "You have to define post");
		LOGGER.debug("Find post");

		try {
			return this.postRepository.save(post);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find postes by group %d", post.getId()), e);
    	}
    }

}