package com.socialgroup.server.service.comparator;

import java.util.Comparator;

import com.socialgroup.server.endpoint.dto.PublicationDto;

public class PublicationOrdered implements Comparator<PublicationDto>{

	@Override
	public int compare(PublicationDto o1, PublicationDto o2) {
		if(o1.getCreated().isBefore(o2.getCreated())) {
			return -1;
		} else if(o1.getCreated().isAfter(o2.getCreated())) {
			return 1;
		} else {
			return 0;
		}
	}

}
