package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgItemNotFoundException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.RequestType;
import com.socialgroup.server.repository.RequestTypeRepository;

@Service
public class RequestTypeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestTypeService.class);

    private final RequestTypeRepository requestTypeRepository;

    public RequestTypeService(final RequestTypeRepository requestTypeRepository) {
        this.requestTypeRepository = requestTypeRepository;
    }

//    public List<RequestType> findAll() {
//        return this.requestTypeRepository.findAll();
//    }

    public RequestType findById(final Long requestTypeId) throws SgException {

    	Validators.isNull(requestTypeId, "You have to define request type");
		LOGGER.debug("Find request type {}", requestTypeId);

		try {
			final Optional<RequestType> requestsType = this.requestTypeRepository.findById(requestTypeId);
	    	if(requestsType.isPresent()) {
	    		return requestsType.get();
	    	} else {
	    		throw new SgItemNotFoundException(String.format("Request type was not found %d", requestTypeId));
	    	}
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find request %d", requestTypeId), e);
    	}
    }

    public RequestType save(final RequestType requestType) throws SgException {

    	Validators.isNull(requestType, "You have to define request type");
		LOGGER.debug("Find request type {}", requestType.getId());

    	try {
    		return this.requestTypeRepository.save(requestType);
    	} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to save request type %d", requestType.getId()), e);
    	}
    }

    public List<RequestType> save(final Set<RequestType> requestsType) throws SgException {

    	Validators.isNull(requestsType, "You have to define requests type");
		LOGGER.debug("Save list of requests type {}", requestsType.size());

    	try {
    		return this.requestTypeRepository.saveAll(requestsType);
    	} catch (final Exception e) {
    		throw throwException("An error has occurred to save requests type", e);
    	}
    }

    public Set<RequestType> createRequestTypes(final Collection<String> requestsType) throws SgException {

		Validators.isNull(requestsType, "You have to define requests type");
		LOGGER.debug("Create list of requests type {}", requestsType.size());

    	try {
    		final Set<RequestType> lstRequestsType = new HashSet<>();
    		for (final String request : requestsType) {
    			final RequestType newRequest = new RequestType();
    			newRequest.setName(request);
    			lstRequestsType.add(newRequest);
    		}
    		return lstRequestsType;
    	} catch (final Exception e) {
    		throw throwException("An error has occurred to create requests type", e);
    	}
    }
}