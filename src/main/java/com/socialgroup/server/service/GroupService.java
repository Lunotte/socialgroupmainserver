package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgItemNotFoundException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.repository.GroupRepository;

@Service
public class GroupService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GroupService.class);

    private final GroupRepository groupRepository;

    public GroupService(final GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

//    public Collection<Group> findAll() {
//        return this.groupRepository.findAll();
//    }

	public Group findById(final Long groupId) throws SgException  {

        Validators.isNull(groupId, "You have to define group");
		LOGGER.debug("Find the group");

		try {
			final Optional<Group> group = this.groupRepository.findById(groupId);
	        if(group.isPresent()) {
	    		return group.get();
	    	} else {
	    		throw new SgItemNotFoundException(String.format("group was not found %d", groupId));
	    	}
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to group %d", groupId), e);
    	}
	}

	public Collection<Group> findAllById(final Collection<Long> groupIds) throws SgException  {

		LOGGER.debug("Find all groups");

		try {
			final Collection<Group> group = this.groupRepository.findAllByIdIn(groupIds);
	        if(group.isEmpty()) {
	    		throw new SgItemNotFoundException(String.format("group was not found %s", groupIds.toString()));
	    	}
	        return group;
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to group %s", groupIds.toString()), e);
    	}
	}

    public Group save(final Group group) throws SgException{

    	Validators.isNull(group, "You have to define group");
		LOGGER.debug("Save the group");

		try {
			return this.groupRepository.save(group);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to group %d", group.getId()), e);
    	}
    }

	public Collection<Group> findUserGroups(final User user) throws SgException {

		Validators.isNull(user, "You have to define user");
		LOGGER.debug("Save the group");

		try {
			return this.groupRepository.findByUserGroupsUser(user);
    	} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to user %s", user.getUsername()), e);
    	}
	}

//	public void testException(final Long groupId) throws SgException {
//		try {
//			throw new SgItemNotFoundException(String.format("L'identifiant n'a pas été trouvé %d", groupId));
//			// throw new PersistenceException();
//		}catch(final Exception e) {
//			throw SgManagerExceptionRouter.throwException("An error as occured on test exception service", e);
//		}
//	}
}