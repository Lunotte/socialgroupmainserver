package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.exception.SgItemNotFoundException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.Availability;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.repository.AvailabilityRepository;

@Service
public class AvailabilityService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AvailabilityService.class);

    private final AvailabilityRepository availabilityRepository;

    public AvailabilityService(final AvailabilityRepository availabilityRepository) {
        this.availabilityRepository = availabilityRepository;
    }

	public Availability findById(final Long availabilityId) throws SgException, SgInvalidArgumentException {

		Validators.isNull(availabilityId, "You have to define a availability");
		LOGGER.debug("Search availability by id {}", availabilityId);

		try {
			final Optional<Availability> availability = this.availabilityRepository.findById(availabilityId);
	        if(availability.isPresent()) {
	    		return availability.get();
	    	} else {
	    		throw new SgItemNotFoundException(String.format("Availability was not found %d", availabilityId));
	    	}
		} catch(final Exception e) {
			throw throwException("An error has occurred to find Availability", e);
		}
	}

//	public List<Availability> findByUser(final User user) throws SgException {
//
//		Validators.isNull(user, "You have to define user");
//		LOGGER.debug("Search the user's availabilities {}", user.getId());
//
//		try {
//			return this.availabilityRepository.findByUser(user);
//		} catch(final Exception e) {
//			throw throwException("An error has occurred to find availabilities by user", e);
//		}
//	}

	public List<Availability> findByGroup(final Group group) throws SgException {

		Validators.isNull(group, "You have to define group");
		LOGGER.debug("Search the group availabilities {}", group.getId());

		try {
			return this.availabilityRepository.findBySourceGroupOrderByCreatedDesc(group);
		} catch(final Exception e) {
			throw throwException("An error has occurred to find availabilities by group", e);
		}
	}

    public Availability save(final Availability availability) throws SgException{

    	Validators.isNull(availability, "You have to define availability");
    	LOGGER.debug("Create a new availability {}", availability);

    	try {
    		return this.availabilityRepository.save(availability);
		} catch(final Exception e) {
			throw throwException("An error has occurred to save availability", e);
		}
    }

	public void delete(final Availability availability) throws SgException {

		Validators.isNull(availability, "You have to define availability");
		LOGGER.debug("Delete availability {}", availability.getId());

		try {
			this.availabilityRepository.delete(availability);
		} catch(final Exception e) {
			throw throwException("An error has occurred to findavailability", e);
		}
	}

    public List<Availability> getAvailabilitiesBetweenStartAndEnd(final ZonedDateTime start, final ZonedDateTime end, final Group group, final User user) throws SgException, SgInvalidArgumentException {

    	Validators.isNull(start, "You have to define start date");
    	Validators.isNull(end, "You have to define end date");
    	Validators.isNull(group, "You have to define group");
    	Validators.isNull(user, "You have to define user");
    	LOGGER.debug("Get availability between two dates {} and {} for the group {} and user {}", start, end, group, user);

    	try {
    		return this.availabilityRepository.findByStartLessThanEqualAndEndGreaterThanEqualAndSourceGroupAndSourceUser(end, start, group, user);
		} catch(final Exception e) {
			throw throwException("An error has occurred to get availabilities by period", e);
		}
    }
}