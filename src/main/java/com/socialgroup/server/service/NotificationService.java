package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.exception.SgRulesException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Notification;
import com.socialgroup.server.entity.Source;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.literals.SourceNotificationEnum;
import com.socialgroup.server.repository.NotificationRepository;

@Service
public class NotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

	@Value("${cron.parameter.notification.weeks}")
	private int weeks;

	private final NotificationRepository notificationRepository;

	public NotificationService(final NotificationRepository notificationRepository) {
		super();
		this.notificationRepository = notificationRepository;
	}

	/**
	 * Add new {@link Notification}}
	 * @param message Message to display for the user
	 * @param user
	 * @param group
	 * @throws SgException
	 */
	public void addNotification(final String message, final SourceNotificationEnum sourceNotification, final User user, final Group group) throws SgException {

		Validators.isNull(user, "You have to define user");
		Validators.isNull(group, "You have to define group");
		Validators.isNullOrEmpty(message, "You have to define a message");
		LOGGER.debug("Add a new notification");

		try {
			this.notificationRepository.save(new Notification(message, sourceNotification, new Source(user, group)));
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to new notification with message %s, user %d and group %d", message, user.getId(), group.getId()), e);
    	}

	}

	/**
	 * The notification was viewed by the user
	 * @param user
	 * @param notification
	 * @throws SgException
	 */
	public Notification seenNotification(final Notification notification) throws SgException {

		Validators.isNull(notification, "You have to define notification");
		LOGGER.debug("Update notification {}", notification.getId());

		try {
			if(notification.isSeen()) {
				throw new SgRulesException("This notification has been already seen.");
			}

			notification.setSeen(true);
			return this.notificationRepository.save(notification);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred for update notification for notification %d", notification.getId()), e);
    	}
	}

	/**
	 * Delete the notification
	 * @param notification
	 * @throws SgException
	 */
	public void deleteNotifications(final Collection<Notification> notification) throws SgException {
		Validators.isNull(notification, "You have to define the notification to delete");
		LOGGER.debug("Delete notifications {}", notification.size());

		try {
			this.notificationRepository.deleteAll(notification);
		} catch (final Exception e) {
    		throw throwException("An error has occurred to save the notification", e);
    	}
	}

	/**
	 * Get all notifications seen and seen since more 5 weeks
	 * @return
	 * @throws SgException
	 */
	public Collection<Notification> findNotificationSeen() throws SgException {
		LOGGER.debug("Find notifications with state seen.");

		try {
			return this.notificationRepository.findAllBySeenAndUpdatedBefore(true, ZonedDateTime.now().minusWeeks(weeks));
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find notifications with the state seen", e);
    	}
	}

	/**
	 * Get all notifications
	 * @return
	 * @throws SgException
	 */
	public Page<Notification> notifications(final User user, final Pageable page) throws SgException {
		LOGGER.debug("Find notifications.");

		try {
			return this.notificationRepository.findAllBySourceUser(user, page);
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find notifications with the state seen", e);
    	}
	}



	/**
	 * Find the notification
	 * @param notificationId
	 * @return
	 * @throws SgException
	 */
	public Notification findNotification(final Long notificationId) throws SgException {
		Validators.isNull(notificationId, "You have to define notification");
		LOGGER.debug("Find notification {}", notificationId);

		try {
			final Optional<Notification> notification = this.notificationRepository.findById(notificationId);
	        if(notification.isPresent()) {
	    		return notification.get();
	    	} else {
	    		throw new SgInvalidArgumentException("The notification was not found");
	    	}
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find notification %d", notificationId), e);
    	}
	}

}
