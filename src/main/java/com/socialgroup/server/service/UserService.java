package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgInconnuException;
import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.common.exception.SgRulesException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.endpoint.dto.MemberDto;
import com.socialgroup.server.entity.BlackListUser;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;
import com.socialgroup.server.facade.ICreateUser;
import com.socialgroup.server.facade.IPersonalInformation;
import com.socialgroup.server.facade.IUpdatePersonalInformation;
import com.socialgroup.server.repository.UserRepository;

/**
 * Mock implementation.
 *
 * @author vladimir.stankovic
 *
 * Aug 4, 2016
 */
@Service
public class UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
	 * Update number of vote for delete this person (if a request has been submitted)
	 * @param userConnected
	 * @param groupActif
	 * @param memberDto
	 * @return
	 * @throws SgMasterException
	 */
	public MemberDto callFindByGroupAndUser(final User userConnected, final User user, final BlackListUser blackListUser)  {

		final MemberDto memberDto = new MemberDto(user);
		memberDto.setNbVote(blackListUser.getChoiceUserBlackList().size());
		memberDto.setVotedByMe(blackListUser.getChoiceUserBlackList().stream().anyMatch(m -> m.getUser().equals(userConnected)));

		return memberDto;
	}

    public User findById(final Long userId) throws SgException {
        Validators.isNull(userId, "You have to define the user");
		LOGGER.debug("Find the user {}", userId);

		try {
			final Optional<User> user = this.userRepository.findById(userId);
	        if(user.isPresent()) {
	    		return user.get();
	    	} else {
	    		throw new SgInvalidArgumentException(String.format("The user was not found %d", userId));
	    	}
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find the user %d", userId), e);
    	}
    }

    public Optional<User> findByIdOptional(final Long userId) throws SgException {
        Validators.isNull(userId, "You have to define the user");
		LOGGER.debug("Find the user {}", userId);

		try {
			return this.userRepository.findById(userId);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find the user %d", userId), e);
    	}
    }

    public Optional<User> findByEmailAuth(final String email) throws SgException {

        Validators.isNullOrEmpty(email, "You have to define the email");
		LOGGER.debug("Find the user {}", email);

		try {
			 return this.userRepository.findByEmailAuth(email);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find the user %S", email), e);
    	}
    }

    public Optional<User> getByEmailAuth(final String email) {

		LOGGER.debug("Find the user {}", email);
		return this.userRepository.findByEmailAuth(email);
    }

    public List<User> findAll() throws SgException {
		LOGGER.debug("Find all users");

		try {
			return this.userRepository.findAll();
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find the user", e);
    	}
    }

    /**
     * Find all users in the group in parameter and not blacklisted
     * @param group
     * @return
     * @throws SgException
     */
    public Optional<Collection<User>> findUsersByGroupId(final Group group) throws SgException {
		LOGGER.debug("Find all users");

		try {
			return this.userRepository.findByUserGroupsGroupAndUserGroupsBlackListedEquals(group, ChoiceYesNoEnum.NO);
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find the user", e);
    	}
    }

	public void delete(final Long userId) {
		this.userRepository.deleteById(userId);
	}

	public User save(final User user) {
		return this.userRepository.save(user);
	}

	public void flush() {
		this.userRepository.flush();
	}

	public String encorePassword(final String password) {
		final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(password);
	}

	/**
	 * Create new {@link User}
	 * @param createUser
	 * @return
	 * @throws SgException
	 */
	public void checkUserInformation(final IPersonalInformation personalInformation) throws SgException {
		LOGGER.debug("Check user information");

		try {

			Validators.isNullOrEmptyAndCheckCondition(personalInformation.getFirstname(),
					personalInformation.getFirstname().length() >= 2 && personalInformation.getFirstname().length() <= 60,
					"Firstname should be between 2 and 60 chars.");
			Validators.isNullOrEmptyAndCheckCondition(personalInformation.getLastname(),
					personalInformation.getFirstname().length() >= 2 && personalInformation.getFirstname().length() <= 60,
					"Lastname should be between 2 and 60 chars.");
			Validators.isNullOrEmptyAndCheckCondition(personalInformation.getUsername(),
					personalInformation.getFirstname().length() >= 2 && personalInformation.getFirstname().length() <= 60,
					"Username should be between 2 and 60 chars.");

			LocalDate dateTmp = LocalDate.now();
			dateTmp = dateTmp.minusYears(100);
			if(personalInformation.getBorn() != null && (personalInformation.getBorn().isBefore(dateTmp) || personalInformation.getBorn().isAfter(LocalDate.now()))) {
				throw new SgRulesException("Date of birth seems invalid");
			}

		} catch (final Exception e) {
			throw throwException("An error has occurred to check user information", e);
		}
	}

	/**
	 * Check email has a good pattern
	 * @param email
	 * @throws SgInvalidArgumentException
	 * @throws SgInconnuException
	 */
	public void isValideEmail(final String email) throws SgInvalidArgumentException, SgInconnuException {
		LOGGER.debug("Check if email is valid");

		try {
			final InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (final AddressException e) {
			throw new SgInvalidArgumentException("Email do not respect pattern.");
		} catch (final Exception e) {
			throw new SgInconnuException("A problem to check email address", e);
		}
	}


	public User buildUser(final ICreateUser createUser) {
		LOGGER.debug("Construct new user");

		return new User(createUser.getFirstname(), createUser.getLastname(), createUser.getUsername(), createUser.getBorn(), createUser.getEmail());
	}

	public Optional<User> findByEmail(final String email) throws SgException {
		Validators.isNullOrEmpty(email, "You have to define the email");
		LOGGER.debug("Find user by email {}", email);

		try {
			 return this.userRepository.findByEmail(email);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find user by email %s", email), e);
    	}
	}

	/**
	 * Check if password and others data respect rules
	 * @param updatePersonalInformation
	 * @throws SgException
	 */
	public void updateCheckPersonalInformation(final IUpdatePersonalInformation updatePersonalInformation) throws SgException {
		LOGGER.debug("Check values for personal information");

		try {
			this.checkUserInformation(updatePersonalInformation);

			if(updatePersonalInformation.getPassword() != null && !updatePersonalInformation.getPassword().equals("") &&
				calculatePasswordStrength(updatePersonalInformation.getPassword()) < 7 ) {
					throw new SgRulesException("Your password is not strong enough.");
			}

		} catch (final Exception e) {
			throw throwException("An error has occurred to check update user information", e);
		}
	}

	public void updatePersonalInformation(final User user, final IUpdatePersonalInformation updatePersonalInformation) throws SgException {
		LOGGER.debug("Update personal information");

		try {
			if(updatePersonalInformation.getFirstname() != null && Validators.isNotNullAndNotEmptySoCheckCondition(updatePersonalInformation.getFirstname(),
					updatePersonalInformation.getFirstname().length() >= 2 && updatePersonalInformation.getFirstname().length() <= 60,
					"Firstname should be between 2 and 60 chars.")) {
				user.setFirstname(updatePersonalInformation.getFirstname());
			}
			if(updatePersonalInformation.getLastname() != null && Validators.isNotNullAndNotEmptySoCheckCondition(updatePersonalInformation.getLastname(),
					updatePersonalInformation.getLastname().length() >= 2 && updatePersonalInformation.getLastname().length() <= 60,
					"Lastname should be between 2 and 60 chars.")) {
				user.setLastname(updatePersonalInformation.getLastname());
			}
			if(updatePersonalInformation.getUsername() != null && Validators.isNotNullAndNotEmptySoCheckCondition(updatePersonalInformation.getUsername(),
					updatePersonalInformation.getUsername().length() >= 2 && updatePersonalInformation.getUsername().length() <= 60,
					"Username should be between 2 and 60 chars.")) {
				user.setUsername(updatePersonalInformation.getUsername());
			}

			LocalDate dateTmp = LocalDate.now();
			dateTmp = dateTmp.minusYears(100);
			if(updatePersonalInformation.getBorn() != null) {
				if(updatePersonalInformation.getBorn().isBefore(dateTmp) || updatePersonalInformation.getBorn().isAfter(LocalDate.now())) {
					throw new SgRulesException("Date of birth seems invalid");
				}
				user.setBorn(updatePersonalInformation.getBorn());
			}

			// user.setAvatar(updatePersonalInformation.getAvatar()); // TODO


			if(updatePersonalInformation.getPassword() != null && !updatePersonalInformation.getPassword().equals("")) {
				if(calculatePasswordStrength(updatePersonalInformation.getPassword()) < 7 ) {
					throw new SgRulesException("Your password is not strong enough.");
				}
				user.setPassword(this.encorePassword(updatePersonalInformation.getPassword()));
			}

			save(user);

		} catch (final Exception e) {
			throw throwException("An error has occurred to check update user information", e);
		}
	}

	private static int calculatePasswordStrength(final String password){

	    int iPasswordScore = 0;

	    if( password.length() < 8 ) {
			return 0;
		} else if( password.length() >= 10 ) {
			iPasswordScore += 2;
		} else {
			iPasswordScore += 1;
		}

	    /*
	    * if password contains 2 digits, add 2 to score.
	    * if contains 1 digit add 1 to score
	    */
	    if( password.matches("(?=.*[0-9].*[0-9]).*") ) {
			iPasswordScore += 2;
		} else if ( password.matches("(?=.*[0-9]).*") ) {
			iPasswordScore += 1;
		}

	    //if password contains 1 lower case letter, add 2 to score
	    if( password.matches("(?=.*[a-z]).*") ) {
			iPasswordScore += 2;
		}

	    /*
	    * if password contains 2 upper case letters, add 2 to score.
	    * if contains only 1 then add 1 to score.
	    */
	    if( password.matches("(?=.*[A-Z].*[A-Z]).*") ) {
			iPasswordScore += 2;
		} else if( password.matches("(?=.*[A-Z]).*") ) {
			iPasswordScore += 1;
		}

	    /*
	    * if password contains 2 special characters, add 2 to score.
	    * if contains only 1 special character then add 1 to score.
	    */
	    if( password.matches("(?=.*[~!@#$%^&*()_-].*[~!@#$%^&*()_-]).*") ) {
			iPasswordScore += 2;
		} else if( password.matches("(?=.*[~!@#$%^&*()_-]).*") ) {
			iPasswordScore += 1;
		}

	    return iPasswordScore;
	}

}
