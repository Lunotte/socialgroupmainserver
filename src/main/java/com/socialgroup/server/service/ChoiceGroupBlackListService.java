package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.ChoiceGroupBlackList;
import com.socialgroup.server.repository.ChoiceGroupBlackListRepository;

@Service
public class ChoiceGroupBlackListService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChoiceGroupBlackListService.class);

	private final ChoiceGroupBlackListRepository choiceGroupBlackListRepository;

	public ChoiceGroupBlackListService(final ChoiceGroupBlackListRepository choiceGroupBlackListRepository) {
		this.choiceGroupBlackListRepository = choiceGroupBlackListRepository;
	}

	/**
	 * Save or Update ChoiceUserBlackList
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public ChoiceGroupBlackList save(final ChoiceGroupBlackList choiceGroupBlackList) throws SgException{

		Validators.isNull(choiceGroupBlackList, "You have to define choice for blackListed group");
		LOGGER.debug("Save the choice for blackListed group");

		try {
			return this.choiceGroupBlackListRepository.save(choiceGroupBlackList);

		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to choice vote for blacklisted group %d", choiceGroupBlackList.getBlackListGroup().getId()), e);
    	}
    }

}
