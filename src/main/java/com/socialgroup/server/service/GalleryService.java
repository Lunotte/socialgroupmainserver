package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.Gallery;
import com.socialgroup.server.repository.GalleryRepository;

@Service
public class GalleryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GalleryService.class);

    private final GalleryRepository galleryRepository;

    public GalleryService(final GalleryRepository galleryRepository) {
        this.galleryRepository = galleryRepository;
    }

    public List<Gallery> findAll() throws SgException {

		LOGGER.debug("Find all galleries");

		try {
			return this.galleryRepository.findAll();
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find all galleries", e);
    	}
    }

	public Gallery findById(final Long galleryId) throws Exception {

        Validators.isNull(galleryId, "You have to define gallery");
		LOGGER.debug("Find gallery {}", galleryId);

		try {
			final Optional<Gallery> gallery = this.galleryRepository.findById(galleryId);
	        if(gallery.isPresent()) {
	    		return gallery.get();
	    	} else {
	    		throw new SgInvalidArgumentException("The gallery was not found");
	    	}
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find gallery %d", galleryId), e);
    	}
	}

    public Gallery save(final Gallery gallery) throws SgException{

    	Validators.isNull(gallery, "You have to define gallery");
 		LOGGER.debug("Save gallery {}", gallery.getId());

 		try {
 			return this.galleryRepository.save(gallery);
 		} catch (final Exception e) {
     		throw throwException(String.format("An error has occurred to save gallery %d", gallery.getId()), e);
     	}
    }

	public void delete(final Long galleryId) throws SgException {
		Validators.isNull(galleryId, "You have to define gallery");
 		LOGGER.debug("Delete gallery {}", galleryId);

 		try {
 			this.galleryRepository.deleteById(galleryId);
 		} catch (final Exception e) {
     		throw throwException(String.format("An error has occurred to delete gallery %d", galleryId), e);
     	}
	}
}