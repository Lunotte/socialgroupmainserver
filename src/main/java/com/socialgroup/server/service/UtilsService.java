package com.socialgroup.server.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.entity.literals.SourceNotificationEnum;

@Service
public class UtilsService {

	private static UserService databaseUserService;
	private static NotificationService notificationService;

	private UtilsService(final UserService userService, final NotificationService notificationService) {
		this.databaseUserService = userService;
		this.notificationService = notificationService;
	}

	public static User getUser(final Long userId) throws SgException {
		final User user = databaseUserService.findById(userId);

		if(user == null) {
			throw new SgInvalidArgumentException("The User was not found");
		}

		return user;
	}

	/**
	 * Get the active group
	 * @param user Connected {@link User}
	 * @return {@link Group}
	 * @throws SgException
	 */
	public static Group getGroupActif(final User user) throws SgException {
		final Optional<UserGroup> userGroup = groupActif(user);

		if(userGroup.isPresent()) {
			return userGroup.get().getGroup();
		} else {
			throw new SgInvalidArgumentException("An error was occurred to find the active group");
		}
	}

	private static Optional<UserGroup> groupActif(final User user) {
		return user.getUserGroups().stream()
									.max(Comparator.comparing(UserGroup::getLastConnextion));
	}

	/**
	 * Find all members in the group
	 *
	 * @param user
	 * @return
	 * @throws SgException
	 */
	private static Collection<User> membersGroup(final User user) throws SgException {
		final Optional<Collection<User>> users = databaseUserService.findUsersByGroupId(user.getGroup());
		if (users.isPresent()) {
			return users.get().stream().filter(member -> !member.equals(user)).collect(Collectors.toList());
		} else {
			return Collections.emptyList();
		}
	}

	/**
	 * Notified members in the group about a news
	 * @param user
	 * @param message
	 * @param sourceNotification
	 * @throws SgException
	 */
	public static void notifiedMembersGroup(final User user, final String message, final SourceNotificationEnum sourceNotification) throws SgException {
		final Collection<User> users = membersGroup(user);

		if(!users.isEmpty()) {
			for (final User member : users) {
				notificationService.addNotification(message, sourceNotification, member, member.getGroup());
			}
		}
	}
}
