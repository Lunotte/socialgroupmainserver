package com.socialgroup.server.service;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgRulesException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.entity.BlackListGroup;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.repository.BlackListGroupRepository;

@Service
public class BlackListGroupService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BlackListGroupService.class);

	private final BlackListGroupRepository blackListGroupRepository;

	public BlackListGroupService(final BlackListGroupRepository blackListGroupRepository) {
		this.blackListGroupRepository = blackListGroupRepository;
	}

	public Optional<BlackListGroup> findByGroup(final Group group) throws SgException{

		Validators.isNull(group, "You have to define group");
		LOGGER.debug("Search BlackListGroup by group {}", group.getId());

		try {
			return this.blackListGroupRepository.findByGroup(group);
		} catch (final Exception e) {
    		throw throwException(String.format("An error has occurred to find the group %d", group.getId()), e);
    	}
    }

	public void checkAlreadyVoted(final User userConnected, final Group groupConcerned) throws SgException {

		LOGGER.debug("Check whether user have already vote for this user and group");

		try {
			final Optional<BlackListGroup> blackList = this.blackListGroupRepository.findByGroupAndChoiceGroupBlackListUserEquals(groupConcerned, userConnected);
			if(blackList.isPresent()) {
				throw new SgRulesException("You can not voted twice");
			}
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find data", e);
    	}
	}

	/**
	 * Save or Update BlackListGroup
	 * @param userGroup
	 * @return UserGroup updated
	 * @throws Exception
	 */
	public BlackListGroup save(final BlackListGroup blackListGroup) throws SgException{

		Validators.isNull(blackListGroup, "You have to define blackListed group");
		LOGGER.debug("Save the blackListGroup {}", blackListGroup.getId());

		try {
			return this.blackListGroupRepository.save(blackListGroup);
		} catch (final Exception e) {
    		throw throwException("An error has occurred to save the blackListed Group", e);
    	}
    }

	/**
	 * Get all temporaries group
	 * @return
	 * @throws SgException
	 */
	private Collection<BlackListGroup> getAllBlackListTemporary() throws SgException {

		LOGGER.debug("Get all blackListGroups");

		try {
			return this.blackListGroupRepository.findAllByDateLimitBefore(ZonedDateTime.now());
		} catch (final Exception e) {
    		throw throwException("An error has occurred to get list of temporary blackListed Group", e);
    	}
	}

	/**
	 * Delete the group blacklist
	 * @param blackListGroup
	 * @throws SgException
	 */
	private void delete(final BlackListGroup blackListGroup) throws SgException {

		Validators.isNull(blackListGroup, "You have to define blackListed group");
		LOGGER.debug("Delete the blackListGroup {}", blackListGroup.getId());

		try {
			this.blackListGroupRepository.delete(blackListGroup);
		} catch (final Exception e) {
    		throw throwException("An error has occurred to delete blackListed Group", e);
    	}
	}

	/**
	 * Get all black listed user and verify whether disable of the group
	 * @throws SgException
	 */
	public void checkBlackListGroupAndDelete() throws SgException {
		LOGGER.debug("Check blackListed users");
		try {
			final Collection<BlackListGroup> tempBlackList = this.getAllBlackListTemporary();
			for (final BlackListGroup blackListGroup : tempBlackList) {
				if(blackListGroup.canBeBlackListed()) {
					this.delete(blackListGroup);
				}
			}
		} catch (final Exception e) {
			throw throwException("An error has occurred to check black listed user", e);
		}
	}

}
