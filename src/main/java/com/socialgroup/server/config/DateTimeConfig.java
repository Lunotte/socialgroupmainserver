package com.socialgroup.server.config;

import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.util.StdDateFormat;

@Configuration
class DateTimeConfig {

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer()
    {
       return new Jackson2ObjectMapperBuilderCustomizer()
       {
          @Override
          public void customize(final Jackson2ObjectMapperBuilder builder)
          {
        	  builder.dateFormat(new StdDateFormat());
          }
       };
    }
}
