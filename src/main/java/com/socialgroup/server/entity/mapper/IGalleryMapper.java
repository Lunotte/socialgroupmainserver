package com.socialgroup.server.entity.mapper;

import java.util.List;

import com.socialgroup.server.endpoint.dto.GalleryDto;
import com.socialgroup.server.entity.Gallery;

public interface IGalleryMapper{

	GalleryDto galleryToGalleryDto(Gallery s);

	Gallery galleryDtoToGallery(GalleryDto galleryDto);

	List<GalleryDto> galleryToGalleryDto(List<Gallery> gallery);

	List<Gallery> galleryDtoToGallery(List<GalleryDto> galleryDto);

}
