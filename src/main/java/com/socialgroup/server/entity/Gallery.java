package com.socialgroup.server.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sg_galleries")
public class Gallery implements Serializable {

	private static final long serialVersionUID = 8630900093789688066L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "path")
	private String path;

	/*
	 * @ManyToOne
	 *
	 * @JoinColumn(name = "user_id")
	 *
	 * @JsonIgnoreProperties("galleries") private User user;
	 *
	 * @ManyToOne
	 *
	 * @JoinColumn(name = "groupId")
	 *
	 * @JsonIgnoreProperties("galleries") private Group group;
	 */

	/*
	 * @ManyToMany(mappedBy = "galleries") //@JsonIgnore private List<User> users;
	 */

	public Gallery() {
	}

	public Gallery(final Long id, final String name, final String description, final String path) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.path = path;
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getPath() {
		return path;
	}

	public void setPath(final String path) {
		this.path = path;
	}

	/*
	 * public User getUser() { return user; }
	 *
	 * public void setUser(User user) { this.user = user; }
	 *
	 * public Group getGroup() { return group; }
	 *
	 * public void setGroup(Group group) { this.group = group; }
	 */

}
