package com.socialgroup.server.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
public abstract class AbstractDate {

	@Column(nullable=false)
	@CreationTimestamp
	private ZonedDateTime created;

	@Column(nullable=false)
	@UpdateTimestamp
	private ZonedDateTime updated;

	public AbstractDate() {
		super();
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(final ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(final ZonedDateTime updated) {
		this.updated = updated;
	}


}
