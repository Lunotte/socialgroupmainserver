package com.socialgroup.server.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.RequestTypeDto;

@Entity
@Table(name="sg_request_types")
public class RequestType {

	@Id @Column(name="id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name="name")
    private String name;

    /*@JsonIgnore
    @OneToMany(mappedBy = "requestType")
    private Set<Request> requests = new HashSet<>();*/
    //@JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
	private Request request;

	public RequestType() {
	}

	public RequestType(final Long id, final String name) {
		this.id = id;
		this.name = name;
	}

	public RequestType(final RequestTypeDto requestTypeDto) {
		this.setId(requestTypeDto.getId());
		this.setName(requestTypeDto.getName());
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Request getRequests() {
		return request;
	}

	public void setRequests(final Request request) {
		this.request = request;
	}

}
