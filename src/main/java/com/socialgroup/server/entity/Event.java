package com.socialgroup.server.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.endpoint.dto.EventDto;

@Entity
@Table(name="sg_events")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Event extends AbstractDate implements Serializable{

	private static final long serialVersionUID = 4706073162284515350L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(updatable = false, nullable = false, unique = true)
    protected Long id;

    @Column(name="start_time", nullable=true)
    protected ZonedDateTime start;

    @Column(name="end_time", nullable=true)
    protected ZonedDateTime end;

    @OneToOne
    protected Post post;

    @Embedded
    protected Source source;

	public Event() {
		super();
	}

	public Event(final Event event) {
		super();
		this.id = event.getId();
		this.start = event.getStart();
		this.end = event.getEnd();
		this.source.setUser(new User(event.getSource().getUser()));
		this.source.setGroup(new Group(event.getSource().getGroup()));
		/*this.created = event.getCreated();
		this.updated = event.getUpdated();*/
	}

	public Event(final EventDto eventDto) {
		super();
		this.id = eventDto.getId();
		this.start = eventDto.getStart();
		this.end = eventDto.getEnd();
		this.source.setUser(new User(eventDto.getUser()));
		this.source.setGroup(new Group(eventDto.getGroup()));
	}

	public Event(final AvailabilityDto availabilityDto) {
		super();
		this.id = availabilityDto.getId();
		this.start = availabilityDto.getStart();
		this.end = availabilityDto.getEnd();
		this.source.setUser(new User(availabilityDto.getUser()));
		this.source.setGroup(new Group(availabilityDto.getGroup()));
	}

	public Event(final Long id, final ZonedDateTime start, final ZonedDateTime end, final User user, final Group group) {
		super();
		this.id = id;
		this.start = start;
		this.end = end;
		this.source.setUser(user);
		this.source.setGroup(group);
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(final ZonedDateTime start) {
		this.start = start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(final ZonedDateTime end) {
		this.end = end;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(final Source source) {
		this.source = source;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(final Post post) {
		this.post = post;
	}

}
