package com.socialgroup.server.entity;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;

@Entity
@Table(name="sg_black_list_user")
public class BlackListUser extends BlackList{

	private static final long serialVersionUID = 5654891132844326837L;

	@Embedded
	private Source source;

	@OneToMany(mappedBy="blackList", orphanRemoval=true)
	private Set<ChoiceUserBlackList> choiceUserBlackList = new HashSet<>();

	public BlackListUser() {
		super();
	}

	public BlackListUser(final ZonedDateTime dateLimit, final Source source) {
		super(dateLimit);
		this.source = source;
	}

	public Set<ChoiceUserBlackList> getChoiceUserBlackList() {
		return choiceUserBlackList;
	}

	public void setChoiceUserBlackList(final Set<ChoiceUserBlackList> choiceUserBlackList) {
		this.choiceUserBlackList = choiceUserBlackList;
	}

	public void addChoiceUserBlackList(final ChoiceUserBlackList choiceUserBlackList) {
		if(!this.choiceUserBlackList.contains(choiceUserBlackList)) {
			this.choiceUserBlackList.add(choiceUserBlackList);
		}
	}

	public Source getSource() {
		return source;
	}

	public void setSource(final Source source) {
		this.source = source;
	}

	@Override
	public boolean canBeBlackListed() {
		final long choiceYes = this.getChoiceUserBlackList().stream().filter(c -> c.getChoice().equals(ChoiceYesNoEnum.YES)).count();
		final long choiceNo = this.getChoiceUserBlackList().stream().filter(c -> c.getChoice().equals(ChoiceYesNoEnum.NO)).count();

		return (choiceYes > choiceNo);
	}

}
