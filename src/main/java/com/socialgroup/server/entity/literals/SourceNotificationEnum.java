package com.socialgroup.server.entity.literals;

public enum SourceNotificationEnum {
	SYSTEM,
	USER
}
