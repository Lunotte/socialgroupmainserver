package com.socialgroup.server.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class Source implements Serializable{

	private static final long serialVersionUID = -2569186350902817747L;

	@ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name="group_id", nullable = false)
    private Group group;

	public Source() {
		super();
	}

	public Source(final User user, final Group group) {
		super();
		this.user = user;
		this.group = group;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(final Group group) {
		this.group = group;
	}

}
