package com.socialgroup.server.entity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.socialgroup.server.entity.literals.SourceNotificationEnum;

@Entity
@Table(name="sg_notification")
public class Notification extends AbstractDate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(updatable = false, nullable = false, unique = true)
    private Long id;

	@Column(nullable=false)
	private String message;

	@Embedded
	private Source source;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private SourceNotificationEnum sourceNotification;

	@Column(nullable=false, columnDefinition = "boolean default false")
	private boolean seen;

	public Notification(final String message, final SourceNotificationEnum sourceNotification, final Source source) {
		super();
		this.message = message;
		this.sourceNotification = sourceNotification;
		this.source = source;
	}

	public Notification() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(final Source source) {
		this.source = source;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(final boolean seen) {
		this.seen = seen;
	}

	/**
	 * @return the sourceNotification
	 */
	public SourceNotificationEnum getSourceNotification() {
		return sourceNotification;
	}

	/**
	 * @param sourceNotification the sourceNotification to set
	 */
	public void setSourceNotification(final SourceNotificationEnum sourceNotification) {
		this.sourceNotification = sourceNotification;
	}

}
