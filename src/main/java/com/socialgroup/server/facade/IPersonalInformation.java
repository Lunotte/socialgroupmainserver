package com.socialgroup.server.facade;

import java.time.LocalDate;

public interface IPersonalInformation {

	public LocalDate getBorn();

	public String getFirstname();

	public String getLastname();

	public String getUsername();
}
