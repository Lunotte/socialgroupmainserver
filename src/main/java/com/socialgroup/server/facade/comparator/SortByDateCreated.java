package com.socialgroup.server.facade.comparator;

import java.util.Comparator;

import com.socialgroup.server.endpoint.dto.PublicationDto;

public class SortByDateCreated implements Comparator<PublicationDto>{

	@Override
	public int compare(PublicationDto o1, PublicationDto o2) {
		return o2.getCreated().compareTo(o1.getCreated());
	}

}
