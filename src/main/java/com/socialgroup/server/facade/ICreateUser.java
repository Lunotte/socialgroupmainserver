package com.socialgroup.server.facade;

import java.util.Set;

public interface ICreateUser extends IPersonalInformation{

	public String getEmail();

	public Set<Long> getGroups();
}
