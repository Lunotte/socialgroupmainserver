package com.socialgroup.server.facade;

import static com.socialgroup.common.exception.SgMasterManagerExceptionRouter.throwException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.common.DateUtils;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.CreatePostJson;
import com.socialgroup.server.endpoint.dto.CommentDto;
import com.socialgroup.server.endpoint.dto.PostDto;
import com.socialgroup.server.endpoint.dto.PublicationDto;
import com.socialgroup.server.endpoint.dto.RequestDto;
import com.socialgroup.server.entity.Comment;
import com.socialgroup.server.entity.CommentSurvey;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Post;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.RequestType;
import com.socialgroup.server.entity.Source;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.literals.SourceNotificationEnum;
import com.socialgroup.server.facade.comparator.SortByDateCreated;
import com.socialgroup.server.service.CommentService;
import com.socialgroup.server.service.CommentSurveyService;
import com.socialgroup.server.service.PostService;
import com.socialgroup.server.service.RequestService;
import com.socialgroup.server.service.RequestTypeService;
import com.socialgroup.server.service.UtilsService;
import com.socialgroup.server.service.comparator.PublicationOrdered;

@Service
public class PublicationFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(PublicationFacade.class);

	private final PostService postService;
	private final RequestService requestService;
	private final RequestTypeService requestTypeService;
	private final CommentService commentService;
	private final CommentSurveyService commentSurveyService;

	@Value("${publication.page.element}")
	private int elementByPage;

	public PublicationFacade(final PostService postService, final RequestService requestService, final RequestTypeService requestTypeService, final CommentService commentService,
			final CommentSurveyService commentSurveyService) {
		super();
		this.postService = postService;
		this.requestService = requestService;
		this.requestTypeService = requestTypeService;
		this.commentService = commentService;
		this.commentSurveyService = commentSurveyService;
	}


	@Transactional(readOnly=true)
	public Page<PublicationDto> getPageablePublications(@AuthUser final UserGroupInformation userGroup, final int page) throws SgMasterException {

		LOGGER.debug("Get all publications");

		try {
			final Group group = userGroup.getGroup();

			final Sort sort = Sort.by(Direction.DESC, "created");
			final Pageable pageRequest = PageRequest.of(page, this.elementByPage, sort);
			final Page<Post> posts =  this.postService.findAllPageable(group, pageRequest);

			final List<PublicationDto> publicationsDto = new ArrayList<>();
			publicationsDto.addAll(posts.getContent().stream().filter(p -> p.getRequest() == null)
							.map(p -> new PublicationDto(new PostDto(p)))
							.collect(Collectors.toList()));

			publicationsDto.addAll(posts.getContent().stream().filter(p -> p.getRequest() != null)
							.map(r -> new PublicationDto(new RequestDto(r.getRequest())))
							.collect(Collectors.toList()));

			publicationsDto.stream().sorted(new PublicationOrdered()).collect(Collectors.toList());
			return new PageImpl<>(publicationsDto, pageRequest, posts.getTotalElements());
		} catch (final Exception e) {
			throw throwException("An error was occurred to get publications", e);
		}
	}



	@Transactional(readOnly=true)
	public List<PublicationDto> getAllPublications(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {

		LOGGER.debug("Get all publications");

		try {
			final Group group = userGroup.getGroup();

			final List<Post> posts =  this.postService.findAllOrderByDate(group);
			final List<Request> requests = this.requestService.findAllOrderByDate(group);
			final List<PublicationDto> publicationsDto = new ArrayList<>();

			for (final Post post : posts) {
				publicationsDto.add(new PublicationDto(new PostDto(post)));
			}

			for (final Request request : requests) {
				final Optional<PublicationDto> publicationDto = publicationsDto.stream().filter(p -> request.getPost() != null && p.getPost().getId().equals(request.getPost().getId())).findFirst();
				if(publicationDto.isPresent()) {
					publicationsDto.remove(publicationDto.get());
					publicationsDto.add(new PublicationDto(new RequestDto(request)));
				}
			}

			Collections.sort(publicationsDto, new SortByDateCreated());

			return publicationsDto;
		} catch (final Exception e) {
			throw throwException("An error was occurred to get publications", e);
		}
	}

	/**
	 * User vote about a survey
	 * @param userGroup
	 * @param requestId Request concerned
	 * @param requestType Choice choosed by user
	 * @return
	 * @throws SgMasterException
	 */
	@Transactional
	public PublicationDto checkRequest(@AuthUser final UserGroupInformation userGroup, final Long requestId, final Long requestType) throws SgMasterException{

		Validators.isNull(requestId, "You have to define a request choo");
		Validators.isNull(requestType, "You have to define a request type");
		LOGGER.debug("User select requestype about survey");

		try {
			final Request request = this.requestService.findById(requestId);
			final User user = userGroup.getUser();

			final Optional<CommentSurvey> comment = request.getCommentSurvey().stream()
					.filter(c ->
						c.getUser().getId().equals(user.getId())
						&& c.getRequestType().getId().equals(requestType)).findFirst();

			if(comment.isPresent()) {
				this.commentSurveyService.remove(comment.get());
				request.removeCommentSurvey(comment.get());
			} else {
				final CommentSurvey commentSurvey = new CommentSurvey();
				commentSurvey.setUser(user);
				commentSurvey.setRequest(request);
				commentSurvey.setRequestType(this.requestTypeService.findById(requestType));

				this.commentSurveyService.save(commentSurvey);
				request.addCommentSurvey(commentSurvey);
			}
			this.requestService.save(request);

			if(request.isSurvey()) {
				UtilsService.notifiedMembersGroup(user, String.format("%s voted about a survey.",
						user.getFirstname()), SourceNotificationEnum.USER);
			} else {
				UtilsService.notifiedMembersGroup(user, String.format("%s voted about request begin %s and finish %s.",
				user.getFirstname(), DateUtils.getStringFromZonedDateTime(request.getStart()),
				DateUtils.getStringFromZonedDateTime(request.getEnd())), SourceNotificationEnum.USER);
			}
			return new PublicationDto(new RequestDto(request));
		} catch (final Exception e) {
			throw throwException("An error was occurred during accepted vote", e);
		}
	}

	/**
	 * Create a new {@link Post}
	 * @param userGroup
	 * @param post
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public PublicationDto createPost(@AuthUser final UserGroupInformation userGroup, final CreatePostJson post) throws SgMasterException {

		Validators.isNull(post, "You have to define a post to create");
		LOGGER.debug("Create post");

		try {
			if(post.getMessage().isEmpty()) {
				throw new SgInvalidArgumentException("Le message ne doit pas êter vide");
			}

			final User user = userGroup.getUser();
			final Group group = userGroup.getGroup();

			final Post poste = new Post();
			poste.setMessage(post.getMessage());
			poste.setUser(user);
			poste.setGroup(group);

			final Post createdPost = postService.save(poste);

			UtilsService.notifiedMembersGroup(user, String.format("%s created a new post",
					user.getFirstname()), SourceNotificationEnum.USER);

			if(post.isRequest()) {

				final Request request = this.requestService.createRequests(createdPost, post, new Source(user, group));

				Set<RequestType> requestsType = new HashSet<>();
				if(post.isSurvey()) {
					requestsType = this.requestTypeService.createRequestTypes(post.getRequestsType());
					request.setRequestTypes(requestsType);
					this.requestTypeService.save(requestsType);
				}

				this.requestService.save(request);

				if(request.isSurvey()) {
					UtilsService.notifiedMembersGroup(user, String.format("%s voted about a survey.",
							user.getFirstname()), SourceNotificationEnum.USER);
				} else {
					UtilsService.notifiedMembersGroup(user, String.format("%s voted a new request from %s to %s.",
							user.getFirstname(), DateUtils.getStringFromZonedDateTime(request.getStart()),
							DateUtils.getStringFromZonedDateTime(request.getEnd())), SourceNotificationEnum.USER);
				}

				return new PublicationDto(new RequestDto(request));
			}

			return new PublicationDto(new PostDto(poste));

		} catch (final Exception e) {
			throw throwException("An error was occurred during creating post", e);
		}
	}

	/**
	 * Update {@link Post}
	 * @param id
	 * @param post
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public PublicationDto updatePost(@AuthUser final UserGroupInformation userGroup, final Long id, final CreatePostJson post) throws SgMasterException {

		Validators.isNull(id, "You have to define a post to update");
		Validators.isNull(post, "You have to define a post");
		LOGGER.debug("Update post {}", id);

		try {
			final User user = userGroup.getUser();

			if(post.getMessage().isEmpty()) {
				throw new SgInvalidArgumentException("Le message ne doit pas êter vide");
			}

			final Request request = this.requestService.findById(id);
			Validators.compareObjectIsNotEquals(request.getSource().getUser(), user, "This event is not yours.");
			request.getPost().setMessage(post.getMessage());
			request.setDescription(post.getMessage());
			request.setStart(post.getStart());
			request.setEnd(post.getEnd());

			request.removeAllRequestsType();
			final Set<RequestType> requestsType = this.requestTypeService.createRequestTypes(post.getRequestsType());

			for (final RequestType requestType : requestsType) {
				request.addRequestType(requestType);
			}

			this.requestTypeService.save(requestsType);
			this.requestService.save(request);

			UtilsService.notifiedMembersGroup(user, String.format("%s updated an event from %s to %s.",
					user.getFirstname(), DateUtils.getStringFromZonedDateTime(request.getStart()),
					DateUtils.getStringFromZonedDateTime(request.getEnd())), SourceNotificationEnum.USER);
			return new PublicationDto(new RequestDto(request));

		}catch(final Exception e) {
			throw throwException(String.format("An error was occurred during update post %d", id), e);
		}
	}

	@Transactional
	public PublicationDto addRequestType(@AuthUser final UserGroupInformation userGroup, final Long requestId, final String requestType) throws SgMasterException{

		Validators.isNull(requestId, "You have to define a request");
		Validators.isNullOrEmpty(requestType, "You have to define a request type");
		LOGGER.debug("Add request type {} on request {}", requestType, requestId);

		try {
			final User user = userGroup.getUser();
			final Request request = this.requestService.findById(requestId);

			final RequestType newRequestType = new RequestType();
			newRequestType.setName(requestType);
			request.addRequestType(newRequestType);

			this.requestTypeService.save(newRequestType);
			this.requestService.save(request);

			UtilsService.notifiedMembersGroup(user, String.format("%s added request type about an event.",
					user.getFirstname()), SourceNotificationEnum.USER);

			return new PublicationDto(new RequestDto(request));
		} catch (final Exception e) {
			throw throwException(String.format("An error was occurred during adding request type on request %d", requestId), e);
		}
	}

	/**
	 * Update comment from post
	 * @param userGroup
	 * @param postId
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public PublicationDto updateCommentToPost(@AuthUser final UserGroupInformation userGroup, final Long postId, final CommentDto dto) throws SgMasterException {

		Validators.isNull(postId, "You have to define a post");
		Validators.isNull(dto, "You have to define a comment");
		LOGGER.debug("Update post {}", postId);

		try {
			Post post = postService.findById(postId);
			final User user = userGroup.getUser();
			final Comment comment = new Comment();
	    	comment.setMessage(dto.getMessage());
	    	comment.setPost(post);
	    	comment.setUser(user);
	    	post.addComment(this.commentService.save(comment));
	    	post = this.postService.save(post);
			return new PublicationDto(new PostDto(post));
		} catch (final Exception e) {
			throw throwException(String.format("An error was occurred during updating post %d", postId), e);
		}
	}

	/**
	 * Supprimer une Request
	 *
	 * @param requestId
	 * @throws SgMasterException
	 * @throws SgExceptionInvalidArgument
	 * @throws Exception
	 */
	@Transactional
	public void deleteRequest(@AuthUser final UserGroupInformation userGroup, final Long requestId) throws SgMasterException {

		Validators.isNull(requestId, "You have to define a request");
		LOGGER.debug("Delete request {}", requestId);

		try {
			final User user = userGroup.getUser();
			final Request request = this.requestService.findById(requestId);
			Validators.compareObjectIsNotEquals(request.getSource().getUser(), user, "This event is not yours.");
			this.requestService.delete(request);
		} catch (final Exception e) {
			throw throwException("An error was occurred during deleting request", e);
		}
	}

}
