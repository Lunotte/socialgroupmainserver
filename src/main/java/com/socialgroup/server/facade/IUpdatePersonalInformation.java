package com.socialgroup.server.facade;

public interface IUpdatePersonalInformation extends IPersonalInformation{

	public String getPassword();

	public String getAvatar();

}