package com.socialgroup.server.facade;

import static com.socialgroup.common.exception.SgMasterManagerExceptionRouter.throwException;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.common.exception.SgRulesException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.GroupDto;
import com.socialgroup.server.endpoint.dto.MemberDto;
import com.socialgroup.server.entity.BlackListGroup;
import com.socialgroup.server.entity.BlackListUser;
import com.socialgroup.server.entity.ChoiceGroupBlackList;
import com.socialgroup.server.entity.ChoiceUserBlackList;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Source;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.entity.UserGroupKey;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;
import com.socialgroup.server.entity.literals.SourceNotificationEnum;
import com.socialgroup.server.service.BlackListGroupService;
import com.socialgroup.server.service.BlackListUserService;
import com.socialgroup.server.service.ChoiceGroupBlackListService;
import com.socialgroup.server.service.ChoiceUserBlackListService;
import com.socialgroup.server.service.GroupService;
import com.socialgroup.server.service.UserService;
import com.socialgroup.server.service.UtilsService;

@Service
public class GroupFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(GroupFacade.class);

	private final GroupService groupService;
	private final UserService userService;
	private final ChoiceGroupBlackListService choiceGroupBlackListService;
	private final BlackListGroupService blackListGroupService;

	private final ChoiceUserBlackListService choiceUserBlackListService;
	private final BlackListUserService blackListUserService;

	public GroupFacade(final GroupService groupService, final UserService userService,
			final ChoiceGroupBlackListService choiceGroupBlackListService, final BlackListGroupService blackListGroupService,
			final ChoiceUserBlackListService choiceUserBlackListService, final BlackListUserService blackListUserService) {
		this.groupService = groupService;
		this.userService = userService;
		this.choiceGroupBlackListService = choiceGroupBlackListService;
		this.blackListGroupService = blackListGroupService;
		this.choiceUserBlackListService = choiceUserBlackListService;
		this.blackListUserService = blackListUserService;
	}

	/**
	 * Get all groups of the user
	 * @param userGroup
	 * @return
	 * @throws SgMasterException
	 */
	@Transactional(readOnly = true)
	public List<GroupDto> findUserGroups(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {

		LOGGER.debug("Get all groups");

		try {
			final User user = userGroup.getUser();
			final Collection<Group> groups = this.groupService.findUserGroups(user);
			return groups.stream().map(GroupDto::new).collect(Collectors.toList());
    	} catch (final Exception e) {
    		throw throwException("An error was occurred to get groups", e);
    	}
	}

	/**
	 * Update the active group
	 * @param userGroup
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public GroupDto updateGroupActif(@AuthUser final UserGroupInformation userGroup, final Long groupId) throws SgMasterException {

		Validators.isNull(groupId, "You have to define an group");
		LOGGER.debug("Update the active group");

		try {
			final User user = userGroup.getUser();
			final Optional<UserGroup> userGroupOpt = user.getUserGroups().stream().filter(ug -> ug.getGroup().getId().equals(groupId)).findFirst();
			if(!userGroupOpt.isPresent()) {
				throw new SgRulesException(String.format("the group %d was not found", groupId));
			}
			userGroupOpt.get().setLastConnextion(ZonedDateTime.now());
			this.userService.save(user);
			return this.getDefaultGroup(userGroup);

		} catch (final Exception e) {
			throw throwException("An error was occurred to update the active group", e);
    	}
	}

	/**
	 * Create new group
	 * @param userGroup
	 * @param groupDto
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public GroupDto addGroup(@AuthUser final UserGroupInformation userGroup, final GroupDto groupDto) throws SgMasterException {

		Validators.isNull(groupDto, "You have to define an group");
		LOGGER.debug("Get the active group");

		try {
			final User user = userGroup.getUser();

			Group group = new Group();
			group.setName(groupDto.getName());

			group = this.groupService.save(group);
			final UserGroup userGroupImp = new UserGroup(user, group);
			userGroupImp.setId(new UserGroupKey(user.getId(), group.getId()));
			user.addUserGroup(userGroupImp);
			this.userService.save(user);
			group.addUserGroup(userGroupImp);
			this.groupService.save(group);

			return this.updateGroupActif(userGroup, group.getId());
		} catch (final Exception e) {
			throw throwException("An error was occurred to get the active group", e);
		}
	}

	/**
	 * Update default group
	 * @param userGroup
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public GroupDto updateDefaultGroup(@AuthUser final UserGroupInformation userGroup, final Long groupId) throws SgMasterException {

		Validators.isNull(groupId, "You have to define an group");
		LOGGER.debug("Update default group");

		try {
			final User user = userGroup.getUser();
			final Optional<UserGroup> userGroupOpt = user.getUserGroups().stream().filter(ug -> ug.getGroup().getId().equals(groupId)).findFirst();
			if(!userGroupOpt.isPresent()) {
				throw new SgRulesException(String.format("the group %d was not found", groupId));
			}
			user.setGroup(userGroupOpt.get().getGroup());
			return new GroupDto(userService.save(user).getGroup());

		} catch (final Exception e) {
			throw throwException("An error was occurred to update default group", e);
    	}
	}

	@Transactional(readOnly = true)
	public GroupDto getDefaultGroup(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {

		LOGGER.debug("Get default group");

		try {
			return new GroupDto(userGroup.getGroup());
		} catch (final Exception e) {
    		throw throwException("An error was occurred to get default group", e);
    	}
	}


	/**
	 * Someone votes to delete this group for the first time
	 * @param userGroup
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public GroupDto prepareDeleteGroup(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {

		LOGGER.debug("Someone ask to delete the group actif");

		try {
			final User user = userGroup.getUser();
			final Group group = userGroup.getGroup();

			if(user.getUserGroups().stream().noneMatch(ug -> ug.getGroup().equals(group))) {
				throw new SgRulesException("Bad data have been sent, try again.");
			}

			this.blackListGroupService.checkAlreadyVoted(user, group);

			final BlackListGroup blackListGroup = new BlackListGroup(limitDate(), group);
			final ChoiceGroupBlackList choiceGroupBlackList = new ChoiceGroupBlackList(blackListGroup, user, ChoiceYesNoEnum.YES);

			this.blackListGroupService.save(blackListGroup);
			this.choiceGroupBlackListService.save(choiceGroupBlackList);

			UtilsService.notifiedMembersGroup(user, String.format("%s want to delete the group %s .", user.getFirstname(), group.getName()), SourceNotificationEnum.USER);

			return null;

		} catch (final Exception e) {
			throw throwException("An error was occurred to blackList the actif group", e);
    	}
	}

	/**
	 * Someone votes to delete this group
	 * @param userGroup
	 * @param groupId Group concerned
	 * @param vote ChoiceYesNoEnum
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public GroupDto voteToDeleteGroup(@AuthUser final UserGroupInformation userGroup, final ChoiceYesNoEnum vote) throws SgMasterException {

		Validators.isNull(vote, "You have to define a vote");
		LOGGER.debug("Someone vote {} about the group", vote);

		try {
			final User user = userGroup.getUser();
			final Group group = userGroup.getGroup();

			final Optional<BlackListGroup> blackListGroup = this.blackListGroupService.findByGroup(group);

			if(!blackListGroup.isPresent()) {
				throw new SgInvalidArgumentException("The group has not found.");
			}

			this.blackListGroupService.checkAlreadyVoted(user, group);

			final ChoiceGroupBlackList choiceGroupBlackList = new ChoiceGroupBlackList(blackListGroup.get(), user, vote);

			this.choiceGroupBlackListService.save(choiceGroupBlackList);

			return null;

		} catch (final Exception e) {
			throw throwException("An error was occurred to vote this actif group", e);
    	}
	}

	/**
	 * Check all elements are black listed
	 * @param userGroup
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkBlackList() throws SgMasterException {

		LOGGER.debug("Treatment on all black listed elements");

		try {
			this.checkBlackListGroup();
			this.checkBlackListUser();
		} catch (final Exception e) {
			throw throwException("An error was occurred to verify black list group", e);
    	}
	}

	/**
	 * Delete group whether respect condition
	 * @param userGroup
	 * @throws Exception
	 */
	private void checkBlackListGroup() throws SgMasterException {

		LOGGER.debug("Treatment on all black listed group");

		try {
			this.blackListGroupService.checkBlackListGroupAndDelete();
		} catch (final Exception e) {
			throw throwException("An error was occurred to treated black list group", e);
    	}
	}

	/**
	 * Someone votes to disable a member of his group for the first time
	 * @param userGroup
	 * @param groupId
	 * @param userId
	 * @return
	 * @throws SgMasterException
	 */
	@Transactional
	public MemberDto prepareDeleteUserFromGroup(@AuthUser final UserGroupInformation userGroup, final Long userId) throws SgMasterException {
		Validators.isNull(userId, "You have to define an user");
		LOGGER.debug("Someone ask to delete the user {} in the current group", userId);

		try {
			final User user = userGroup.getUser();
			final Group group = userGroup.getGroup();
			final User userToRemove = this.userService.findById(userId);

			// L'utilisateur à supprimer existe bien dans le group passé en paramètre
			final boolean userToRemoveIsPresentInThisGroup = userToRemove.getUserGroups().stream().anyMatch(ug -> ug.getGroup().equals(group));
			final boolean userIsPresentInThisGroup = user.getUserGroups().stream().anyMatch(ug -> ug.getGroup().equals(group));

			if(!userToRemoveIsPresentInThisGroup || !userIsPresentInThisGroup){
				throw new SgRulesException("Bad data have been sent, try again.");
			}

			this.blackListUserService.checkAlreadyVoted(user, userToRemove, group);

			final BlackListUser blackListUser = new BlackListUser(limitDate(), new Source(userToRemove, group));
			final ChoiceUserBlackList choiceUserBlackList = new ChoiceUserBlackList(blackListUser, user, ChoiceYesNoEnum.YES);

			this.blackListUserService.save(blackListUser);
			this.choiceUserBlackListService.save(choiceUserBlackList);

			blackListUser.addChoiceUserBlackList(choiceUserBlackList);

			UtilsService.notifiedMembersGroup(user, String.format("%s want to delete the user %s .", user.getFirstname(), userToRemove.getFirstname()), SourceNotificationEnum.USER);


			return this.userService.callFindByGroupAndUser(user, userToRemove, blackListUser);

		} catch (final Exception e) {
			throw throwException("An error was occurred to blacklist user", e);
    	}
	}

	/**
	 * Someone votes to disable a member of his group
	 * @param userGroup
	 * @param groupId Group concerned
	 * @param userId User to diable (maybe)
	 * @param vote Choice of the user
	 * @return
	 * @throws SgMasterException
	 */
	@Transactional
	public MemberDto voteToDisableUser(@AuthUser final UserGroupInformation userGroup, final Long userId, final ChoiceYesNoEnum vote) throws SgMasterException {

		Validators.isNull(userId, "You have to define an user");
		Validators.isNull(vote, "You have to define a vote");
		LOGGER.debug("Someone vote {} about the user {}", vote, userId);

		try {
			final User user = userGroup.getUser();
			final User userToDisable = this.userService.findById(userId);
			final Group groupConcerned = userGroup.getGroup();

			final BlackListUser blackListUser = this.blackListUserService.findByGroupAndUser(groupConcerned, userToDisable);
			this.blackListUserService.checkAlreadyVoted(user, userToDisable, groupConcerned);

			final ChoiceUserBlackList choiceUserBlackList = new ChoiceUserBlackList(blackListUser, user, vote);
			this.choiceUserBlackListService.save(choiceUserBlackList);

			blackListUser.addChoiceUserBlackList(choiceUserBlackList);

			return this.userService.callFindByGroupAndUser(user, userToDisable, blackListUser);

		} catch (final Exception e) {
			throw throwException(String.format("An error was occurred to vote this user %d", userId), e);
    	}
	}

	/**
	 * Supprime les éléments répondant à la condition
	 * @param userGroup
	 * @throws Exception
	 */
	private void checkBlackListUser() throws SgMasterException {

		LOGGER.debug("Treatment on all black listed group");

		try {
			this.blackListUserService.checkBlackListAndDisableUser();
		} catch (final Exception e) {
			throw throwException("An error was occurred to treated black list group", e);
    	}
	}

	private ZonedDateTime limitDate() {
		ZonedDateTime date = ZonedDateTime.now();
		date = date.plusDays(7L);
		return date;
	}


//	public GroupDto testException() throws SgMasterException {
//		try {
////			final Optional<BlackListGroup> blackListGroup = Optional.empty();
////
////			if(!blackListGroup.isPresent()) {
////				throw new SgItemNotFoundException(String.format("L'identifiant n'a pas été trouvé "));
////			}
//			 this.groupService.testException(null);
//			return null;
//		} catch (final Exception e) {
//    		throw SgMasterManagerExceptionRouter.throwException("Une erreur est survenue sur test exception facade", e);
//    	}
//	}


}
