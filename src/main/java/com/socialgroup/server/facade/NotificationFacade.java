package com.socialgroup.server.facade;

import static com.socialgroup.common.exception.SgManagerExceptionRouter.throwException;

import java.util.Collection;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.common.exception.SgRulesException;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.NotificationDto;
import com.socialgroup.server.entity.Notification;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.service.NotificationService;

@Service
public class NotificationFacade {

	private final NotificationService notificationService;

	@Value("${notification.page.element}")
	private int elementByPage;

	public NotificationFacade(final NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationFacade.class);


	@Transactional(readOnly = true)
	public Page<NotificationDto> notifications(@AuthUser final UserGroupInformation userGroup, final int page) throws SgMasterException {
		LOGGER.debug("Get all notifications.");

		try {
			final Sort sort = Sort.by(Direction.DESC, "created");
			final Pageable pageRequest = PageRequest.of(page, this.elementByPage, sort);
			final User user = userGroup.getUser();
			final Page<Notification> notifications = this.notificationService.notifications(user, pageRequest);

			return new PageImpl<>(notifications.getContent().stream().map(NotificationDto::new).collect(Collectors.toList()),
									pageRequest, notifications.getTotalElements());
		}catch (final Exception e) {
			throw throwException("An error was occurred to update the notification", e);
		}
	}

	/**
	 * The user has seen the notification
	 * @param userGroup
	 * @param notificationId
	 * @throws SgMasterException
	 */
	@Transactional
	public NotificationDto seenNotification(@AuthUser final UserGroupInformation userGroup, final Long notificationId) throws SgMasterException {
		LOGGER.debug("Update notification for seen.");

		try {
			final User user = userGroup.getUser();
			final Notification notification = this.notificationService.findNotification(notificationId);

			// whether the notification is mine and the group is the one I'm connected to
			if(!notification.getSource().getUser().equals(user) || !notification.getSource().getGroup().equals(user.getGroup())) {
				throw new SgRulesException("This notification is not known.");
			}
			return new NotificationDto(this.notificationService.seenNotification(notification));
		}catch (final Exception e) {
			throw throwException("An error was occurred to update the notification", e);
		}
	}

	/**
	 * Get all notifications seen and seen since more 5 weeks
	 * @return
	 * @throws SgException
	 */
	@Transactional
	public void deleteNotificationSeen() throws SgException {
		LOGGER.debug("Find notifications with state seen.");

		try {
			final Collection<Notification> notifications = this.notificationService.findNotificationSeen();
			if(!notifications.isEmpty()) {
				this.notificationService.deleteNotifications(notifications);
			}
		} catch (final Exception e) {
    		throw throwException("An error has occurred to find notifications with the state seen", e);
    	}
	}
}
