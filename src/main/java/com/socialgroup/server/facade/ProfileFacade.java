package com.socialgroup.server.facade;

import static com.socialgroup.common.exception.SgMasterManagerExceptionRouter.throwException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.socialgroup.common.exception.SgException;
import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.common.exception.SgRulesException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.common.email.TLSEmail;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.GroupDto;
import com.socialgroup.server.endpoint.dto.MemberDto;
import com.socialgroup.server.endpoint.dto.ProfilDto;
import com.socialgroup.server.entity.BlackListUser;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Role;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.entity.literals.SourceNotificationEnum;
import com.socialgroup.server.entity.literals.UserRole;
import com.socialgroup.server.entity.literals.UserRole.Id;
import com.socialgroup.server.service.BlackListUserService;
import com.socialgroup.server.service.GroupService;
import com.socialgroup.server.service.NotificationService;
import com.socialgroup.server.service.UserService;

@Service
public class ProfileFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileFacade.class);

	private final UserService userService;
	private final BlackListUserService blackListUserService;
	private final GroupService groupService;
	private final TLSEmail tLSEmail;
	private final NotificationService notificationService;

	public ProfileFacade(final UserService userService, final GroupService groupService, final TLSEmail tLSEmail,
			final NotificationService notificationService, final BlackListUserService blackListUserService) {
		this.userService = userService;
		this.blackListUserService = blackListUserService;
		this.groupService = groupService;
		this.tLSEmail = tLSEmail;
		this.notificationService = notificationService;
	}

	/**
	 * Get information of profil user
	 * @param userGroup
	 * @return
	 * @throws SgMasterException
	 */
	@Transactional(readOnly = true)
	public ProfilDto profil(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {
		LOGGER.debug("Update personal information");

		try {
			final User user = userGroup.getUser();

			final Collection<GroupDto> groups = user.getUserGroups().stream()
					.map(g -> new GroupDto(g.getGroup())).collect(Collectors.toList());

			return new ProfilDto(user.getId(), user.getAvatar(), user.getLastname(), user.getFirstname(),
					user.getUsername(), user.getBorn(), groups);

		}catch (final Exception e) {
			throw throwException("An error was occurred to update personal information.", e);
		}
	}

	/**
	 * Get list of membre in the current group
	 * @param userGroup
	 * @return List of {@link MemberDto}
	 * @throws SgMasterException
	 */
	@Transactional(readOnly = true)
	public List<MemberDto> getUsersInCurrentGroup(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {
		LOGGER.debug("Update personal information");

		try {
			final User user = userGroup.getUser();
			final Group group = userGroup.getGroup();

			final Optional<Collection<User>> usersInMyGroup = this.userService.findUsersByGroupId(group);
			return usersInMyGroup.isPresent() ? usersInMyGroup.get().stream()
					.map(m -> this.buildMember(user, group, m))
					.collect(Collectors.toList()) : Collections.emptyList();

		}
		catch (final Exception e) {
			throw throwException("An error was occurred to update personal information.", e);
		}
	}

	/**
	 * Update number of vote for delete this person (if a request has been submitted)
	 * @param userConnected
	 * @param groupActif
	 * @param memberDto
	 * @return
	 * @throws SgMasterException
	 */
	private MemberDto buildMember(final User userConnected, final Group groupActif, final User user)  {

		try {
			final BlackListUser blackListUser = this.blackListUserService.findByGroupAndUser(groupActif, user);
			return this.userService.callFindByGroupAndUser(userConnected, user, blackListUser);
		} catch (final SgInvalidArgumentException e) {
			LOGGER.info("Not Black listed", e);
			return new MemberDto(user);
		}catch (final SgException e) {
			LOGGER.error("An error was occurred to update number of vote.", e);
			return new MemberDto(user);
		}
	}

	/**
	 * Update personal information of the user
	 * @param userGroup
	 * @param updatePersonalInformation
	 * @throws SgMasterException
	 */
	@Transactional
	public ProfilDto updatePersonalInformation(@AuthUser final UserGroupInformation userGroup, final IUpdatePersonalInformation updatePersonalInformation) throws SgMasterException {
		LOGGER.debug("Update personal information");

		try {
			final User user = userGroup.getUser();
			this.userService.updatePersonalInformation(user, updatePersonalInformation);
			return new ProfilDto(user);
		}catch (final Exception e) {
			throw throwException("An error was occurred to update personal information.", e);
		}
	}

	/**
	 * Create new User
	 * @param userGroup
	 * @param createUser
	 * @throws SgMasterException
	 */
	@Transactional
	public void createUser(@AuthUser final UserGroupInformation userGroup, final ICreateUser createUser) throws SgMasterException {
		LOGGER.debug("Create new user");

		try {
			final User userConnected = userGroup.getUser();

			if(userConnected.getEmail().equalsIgnoreCase(createUser.getEmail())) {
				throw new SgRulesException("You feel alone to add yourself again ?");
			}

			userService.checkUserInformation(createUser);
			Validators.isNullOrEmpty(createUser.getEmail(), "Email should not be empty.");
			userService.isValideEmail(createUser.getEmail());

			final Optional<User> userExist = userService.findByEmail(createUser.getEmail());

			User user = null;
			String password = null;
			if(userExist.isPresent()) {
				user = userExist.get();
			} else {
				user = userService.buildUser(createUser);
				user.setGroup(userGroup.getGroup()); //Groupe par defaut, celui où le user est logué
				password = RandomStringUtils.randomAlphanumeric(12); // A garder de cote pour envoyer l'email
				user.setPassword(userService.encorePassword(password)); // On encode le mot de passe;
				Validators.isEmptyAndCheckCondition(createUser.getGroups(),
						!createUser.getGroups().isEmpty(), "You neeed to choice at least one group.");
			}

			final Collection<Group> groups = groupService.findAllById(createUser.getGroups());
			addGroupToUser(user, groups);
			this.userService.flush();

			if(password == null) {
				notificationService.addNotification("Welcome in this group", SourceNotificationEnum.SYSTEM, user, user.getGroup());
			} else {
				final UserRole userRole = new UserRole(new Id(user.getId(), Role.MEMBER));
				user.addUserRole(userRole);
				tLSEmail.prepareEmail(user.getEmail(), password);
			}
		} catch (final Exception e) {
			throw throwException("An error was occurred to create user", e);
		}
	}

	/**
	 * Add user in a list of group
	 * @param user
	 * @param groups
	 * @throws SgMasterException
	 */
	private void addGroupToUser(final User user, final Collection<Group> groups) throws SgMasterException {
		LOGGER.debug("Add group and role for the new user.");

		try {
			for (final Group group : groups) {
				if(user.getUserGroups().stream().noneMatch(ug -> ug.getGroup().equals(group)) == false) {
					throw new SgRulesException("You cannot add a group already added");
				}
			}
			userService.save(user);

			final Collection<UserGroup> userGroups = new ArrayList<>();
			for (final Group group : groups) {
				userGroups.add(new UserGroup(user, group));
			}
			user.addUserGroups(userGroups);
		} catch (final Exception e) {
			throw throwException("An error was occurred to add group and role", e);
		}
	}

	@Transactional(readOnly = true)
	public boolean isRegisteredUser(final String email) throws SgMasterException {
		LOGGER.debug("Check whether user is registered");

		try {
			return this.userService.findByEmail(email).isPresent();
		}
		catch (final Exception e) {
			throw throwException("An error was occurred to check whether user is registered.", e);
		}
	}

}
