package com.socialgroup.server.facade;

import static com.socialgroup.common.exception.SgMasterManagerExceptionRouter.throwException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.common.exception.SgRulesException;
import com.socialgroup.common.tool.Validators;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.endpoint.dto.CreateAvailability;
import com.socialgroup.server.endpoint.dto.PlanningDto;
import com.socialgroup.server.endpoint.dto.RequestDto;
import com.socialgroup.server.entity.Availability;
import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.Source;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.literals.SourceNotificationEnum;
import com.socialgroup.server.service.AvailabilityService;
import com.socialgroup.server.service.RequestService;
import com.socialgroup.server.service.UtilsService;

@Service
public class PlanningFacade {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanningFacade.class);

	private final RequestService requestService;
	private final AvailabilityService availabilityService;

	public PlanningFacade(final RequestService requestService, final AvailabilityService availabilityService) {
		this.requestService = requestService;
		this.availabilityService = availabilityService;
	}

	@Transactional(readOnly = true)
	public Collection<PlanningDto> getAllPlannings(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {

		LOGGER.debug("Get all plannings");

		final Group group = userGroup.getGroup();

		final List<PlanningDto> planningsDto = new ArrayList<>();

		try {
			final List<Availability> availabilities =  this.availabilityService.findByGroup(group);
			final List<Request> requests = this.requestService.findByGroupAndSurveyIsFalseOrderByCreatedDesc(group);

			for (final Availability availability : availabilities) {
				planningsDto.add(new PlanningDto(new AvailabilityDto(availability)));
			}

			for (final Request request : requests) {
				planningsDto.add(new PlanningDto(new RequestDto(request)));
			}

			//Pageable pag = new pagere
		} catch (final Exception e) {
			throw throwException("An error was occurred to get all plannings", e);
		}

		return planningsDto;
	}

	/**
	 * Create a new Availability
	 *
	 * @param userGroup
	 * @param availability
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public AvailabilityDto postAvailability(@AuthUser final UserGroupInformation userGroup, final CreateAvailability availability) throws SgMasterException {

		Validators.isNull(availability, "You have to define an availability");
		LOGGER.debug("Create an availability");

		final User user = userGroup.getUser();
		final Group group = userGroup.getGroup();

		try {

			final List<Availability> availabilities = this.availabilityService.getAvailabilitiesBetweenStartAndEnd(
					availability.getStart(), availability.getEnd(), group, user);
			if(!availabilities.isEmpty()) {
				throw new SgRulesException("An availability is already present.");
			}

			final Availability anAvailability = new Availability(availability.getType());
			anAvailability.setSource(new Source(user, group));
			anAvailability.setStart(availability.getStart());
			anAvailability.setEnd(availability.getEnd());

			final Availability available = this.availabilityService.save(anAvailability);
			UtilsService.notifiedMembersGroup(user, String.format("%s add a new availability.", user.getFirstname()), SourceNotificationEnum.USER);

			return new AvailabilityDto(available);

		} catch (final Exception e) {
			throw throwException("An error was occurred to create an availability", e);
		}
	}

	@Transactional
	public AvailabilityDto updateAvailability(@AuthUser final UserGroupInformation userGroup, final Long idAvailability, final CreateAvailability availability) throws SgMasterException {

		Validators.isNull(idAvailability, "You have to define an availability to update");
		Validators.isNull(availability, "You have to define an availability");
		LOGGER.debug("Update an availability");

		try {
			final User user = userGroup.getUser();

			final Availability anAvailability = this.availabilityService.findById(idAvailability);
			Validators.compareObjectIsNotEquals(anAvailability.getSource().getUser(), user, "This availability is not yours.");

			anAvailability.setStart(availability.getStart());
			anAvailability.setEnd(availability.getEnd());
			anAvailability.setAvailabilityType(availability.getType());

			final Availability available = this.availabilityService.save(anAvailability);
			UtilsService.notifiedMembersGroup(user, String.format("%s updated an availability.", user.getFirstname()), SourceNotificationEnum.USER);

			return new AvailabilityDto(available);

		} catch (final Exception e) {
			throw throwException("An error as occured to update availability", e);
		}
	}

	@Transactional
	public void deleteAvailability(@AuthUser final UserGroupInformation userGroup, final Long idAvailability) throws SgMasterException {

		Validators.isNull(idAvailability, "You have to define an availability to delete");
		LOGGER.debug("Update an availability");

		try {
			final User user = userGroup.getUser();

			final Availability anAvailability = this.availabilityService.findById(idAvailability);
			Validators.compareObjectIsNotEquals(anAvailability.getSource().getUser(), user, "This availability is not yours.");

			this.availabilityService.delete(anAvailability);
		} catch (final Exception e) {
			throw throwException("An error as occured to delete availability", e);
		}
	}
}
