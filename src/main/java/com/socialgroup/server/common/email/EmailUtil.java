package com.socialgroup.server.common.email;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.socialgroup.common.exception.SgInconnuException;

public class EmailUtil {

	private EmailUtil() {

	}

	/**
	 * Utility method to send simple HTML email
	 * @param session
	 * @param toEmail
	 * @param subject
	 * @param body
	 * @throws SgInconnuException
	 */
	public static void sendEmail(final Session session, final String toEmail, final String subject, final String body) throws SgInconnuException{
		try
	    {
	      final MimeMessage msg = new MimeMessage(session);
	      //set message headers
	      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	      msg.addHeader("format", "flowed");
	      msg.addHeader("Content-Transfer-Encoding", "8bit");

	      msg.setFrom(new InternetAddress("no_reply@example.com", "NoReply-JD"));
	      msg.setReplyTo(InternetAddress.parse("no_reply@example.com", false));
	      msg.setSubject(subject, "UTF-8");

	     // msg.setText(body, "UTF-8");
	      msg.setDataHandler(new DataHandler(new HTMLDataSource(body)));
	      msg.setSentDate(new Date());

	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
    	  Transport.send(msg);

	      System.out.println("EMail Sent Successfully!!");
	    }
	    catch (final Exception e) {
	     throw new SgInconnuException("An error has ocurred to send email.", e);
	    }
	}

	static class HTMLDataSource implements DataSource {

        private final String html;

        public HTMLDataSource(final String htmlString) {
            html = htmlString;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (html == null) {
				throw new IOException("html message is null!");
			}
            return new ByteArrayInputStream(html.getBytes());
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }

        @Override
        public String getContentType() {
            return "text/html";
        }

        @Override
        public String getName() {
            return "HTMLDataSource";
        }
    }
}