package com.socialgroup.server.common.email;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.socialgroup.common.exception.SgInconnuException;

@Service
public class TLSEmail {

	@Value("${email.security.sgEmail}")
	private String sgEmail;

	@Value("${email.security.sgPassword}")
	private String sgPassword;

	/**
	   Outgoing Mail (SMTP) Server
	   requires TLS or SSL: smtp.gmail.com (use authentication)
	   Use Authentication: Yes
	   Port for TLS/STARTTLS: 587
	 * @throws SgInconnuException
	 */
	public void prepareEmail(final String emailUser, final String passwordGenerated) throws SgInconnuException {
		final String fromEmail = sgEmail; //requires valid gmail id
		final String passwordGmail = sgPassword; // correct password for gmail id
		final String toEmail = emailUser; // can be any email id

		final Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		props.put("mail.smtp.port", "587"); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

                //create Authenticator object to pass in Session.getInstance argument
		final Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, passwordGmail);
			}
		};
		final Session session = Session.getInstance(props, auth);

		EmailUtil.sendEmail(session, toEmail,"TLSEmail Testing Subject", "<div style=\"\r\n" +
				"	margin: 0 auto;\r\n" +
				"	padding: 10px;\r\n" +
				"	min-width: 300px;\r\n" +
				"	max-width: 800px;\r\n" +
				"    text-align: left;\r\n" +
				"    color: #292c2e;\r\n" +
				"	background-color: #f2f2f2;\">\r\n" +
				"	\r\n" +
				"	<p>Hey,</p>\r\n" +
				" \r\n" +
				"	<p>If you receive this mail, it means that someone wants to see you in his knowledge group</p>\r\n" +
				"	<p>An account has been created for you. In the link below you will be able to connect with the identifiers provided in this email.</p>\r\n" +
				"	\r\n" +
				"	<div style=\"text-align: center;\">\r\n" +
				"		<span><b>" + emailUser + "</b></span><br/>\r\n" +
				"		<span><b>" + passwordGenerated + "</b></span>\r\n" +
				"	</div>\r\n" +
				"	\r\n" +
				"	<p>Welcome on SocialCenter :<a href=\"https://mdbootstrap.com/education/angular/\"> http://social-center.com</a></p>\r\n" +
				" \r\n" +
				"	<p>Have a nice day.</p>\r\n" +
				"	\r\n" +
				"	<footer style=\"text-align: center; padding: 10px; border: 1px solid white;\">\r\n" +
				"		<div>© 2019 Copyright:\r\n" +
				"		<a href=\"https://mdbootstrap.com/education/angular/\"> social-center.com</a>\r\n" +
				"	  </div>\r\n" +
				"	</footer>\r\n" +
				"</div>");

	}

}
