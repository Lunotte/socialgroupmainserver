package com.socialgroup.server.common.resolver;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.User;

public class UserGroupInformation {

	private User user;
	private Group group;

	UserGroupInformation() { }

	public UserGroupInformation(final User user, final Group group)
	{
		this.user = user;
		this.group = group;
	}

	public User getUser()
	{
		return user;
	}

	public Group getGroup()
	{
		return group;
	}

}
