package com.socialgroup.server.common.resolver;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.socialgroup.server.entity.User;
import com.socialgroup.server.service.UtilsService;

public class AuthUserArgumentResolver implements HandlerMethodArgumentResolver {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthUserArgumentResolver.class);

	private final RestTemplate restTemplate;

	public AuthUserArgumentResolver(final RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public boolean supportsParameter(final MethodParameter parameter) {
		return parameter.getParameterAnnotation(AuthUser.class) != null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object resolveArgument(final MethodParameter parameter, final ModelAndViewContainer mavContainer,
			final NativeWebRequest webRequest, final WebDataBinderFactory binderFactory) throws Exception {

		LOGGER.debug("Get user authenticated.");

		final HttpHeaders headers = new HttpHeaders();
		final String token = webRequest.getHeader("X-Authorization");
		final String serviceAuth = webRequest.getHeader("serviceAuth");
		headers.set("X-Authorization", token);
		final HttpEntity<String> entity = new HttpEntity<>("body", headers);
		final ResponseEntity<Map> values = restTemplate.exchange("http://" + serviceAuth + "/user-group-information", HttpMethod.GET, entity,
				Map.class);

		final Map<String, Integer> value = values.getBody();
		final Integer userId = value.get("user");
		final User user = UtilsService.getUser(userId.longValue());

		return new UserGroupInformation(user, UtilsService.getGroupActif(user));
	}
}
