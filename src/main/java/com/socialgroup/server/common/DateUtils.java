package com.socialgroup.server.common;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {
	
	private DateUtils() {}
	
	private static String pattern = "yyyy-MM-dd";
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
	
	public static String getStringFromZonedDateTime(final ZonedDateTime date) {
		return date.format(formatter);
	}

}
