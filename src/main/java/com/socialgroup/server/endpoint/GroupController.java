package com.socialgroup.server.endpoint;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.GroupDto;
import com.socialgroup.server.endpoint.dto.MemberDto;
import com.socialgroup.server.entity.literals.ChoiceYesNoEnum;
import com.socialgroup.server.facade.GroupFacade;

@RestController
public class GroupController {

	private static final Logger LOGGER = LoggerFactory.getLogger(GroupController.class);

	private final GroupFacade groupFacade;

	public GroupController(final GroupFacade groupFacade) {
		this.groupFacade = groupFacade;
	}

	@GetMapping("/testArgument")
	public String methodeTestArgument(@AuthUser final UserGroupInformation userGroup) {
		LOGGER.info("Test user: {} et group : {}", userGroup.getUser(), userGroup.getGroup());
		return "test " + userGroup.getUser() + " : " + userGroup.getGroup();
	}

	/**
	 * Get all groups where the user is associate
	 * @param userGroup
	 * @return
	 * @throws SgMasterException
	 */
	@GetMapping(value="/groups", produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<GroupDto> userGroups(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {
		return this.groupFacade.findUserGroups(userGroup);
    }

	/**
	 * Post new Group
	 * @param group
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value="/group", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto createGroup(@AuthUser final UserGroupInformation userGroup, @RequestBody final GroupDto groupDto) throws SgMasterException {
		return this.groupFacade.addGroup(userGroup, groupDto);
    }

	/**
	 * Update the group used by the user at this moment (User change group)
	 * @param userGroup
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	@PutMapping(value="/group/{groupId}/actif", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto updateGroupActif(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long groupId) throws SgMasterException {
		return this.groupFacade.updateGroupActif(userGroup, groupId);
    }

	/**
	 * Defined default group for the user
	 * @param userGroup
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	@PutMapping(value="/group/{groupId}/default", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto updateDefaultGroup(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long groupId) throws SgMasterException {
		return this.groupFacade.updateDefaultGroup(userGroup, groupId);
    }

	/**
	 * Get current default group
	 * @param userGroup
	 * @return
	 * @throws SgMasterException
	 */
	@GetMapping(value="/group/default", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto getDefaultGroup(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {
		return this.groupFacade.getDefaultGroup(userGroup);
    }

	/**
	 * Create vote to remove a group
	 * @param userGroup
	 * @return
	 * @throws SgMasterException
	 */
	@PutMapping(value="/group/vote", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto prepareToDeleteGroup(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {
		return this.groupFacade.prepareDeleteGroup(userGroup);
    }

	/**
	 * Vote to remove a group
	 * @param userGroup
	 * @param vote
	 * @return
	 * @throws SgMasterException
	 */
	@PutMapping(value="/group/vote/{vote}", produces=MediaType.APPLICATION_JSON_VALUE)
    public GroupDto voteToDeleteGroup(@AuthUser final UserGroupInformation userGroup, @PathVariable final ChoiceYesNoEnum vote) throws SgMasterException {
		return this.groupFacade.voteToDeleteGroup(userGroup, vote);
    }

	/**
	 * Create vote to remove user
	 * @param userGroup
	 * @param userId
	 * @return
	 * @throws SgMasterException
	 */
	@PutMapping(value="/group/user/{userId}/vote", produces=MediaType.APPLICATION_JSON_VALUE)
    public MemberDto prepareToDeleteUser(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long userId) throws SgMasterException {
		return this.groupFacade.prepareDeleteUserFromGroup(userGroup, userId);
    }

	/**
	 * Vote to remove user
	 * @param userGroup
	 * @param userId
	 * @param vote
	 * @return
	 * @throws SgMasterException
	 */
	@PutMapping(value="/group/user/{userId}/vote/{vote}", produces=MediaType.APPLICATION_JSON_VALUE)
    public MemberDto voteToDeleteUser(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long userId, @PathVariable final ChoiceYesNoEnum vote) throws SgMasterException {
		return this.groupFacade.voteToDisableUser(userGroup, userId, vote);
    }

}
