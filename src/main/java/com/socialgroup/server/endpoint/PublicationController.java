package com.socialgroup.server.endpoint;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.CommentDto;
import com.socialgroup.server.endpoint.dto.PublicationDto;
import com.socialgroup.server.facade.PublicationFacade;

@RestController
public class PublicationController {

	private final PublicationFacade publicationFacade;

	public PublicationController(final PublicationFacade publicationFacade) {
		this.publicationFacade = publicationFacade;
	}

	@GetMapping(value="/publications", produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<PublicationDto> posts(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {
		return publicationFacade.getAllPublications(userGroup);
    }

	@GetMapping(value="/publications/{page}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Page<PublicationDto> postsPageable(@AuthUser final UserGroupInformation userGroup, @PathVariable final int page) throws SgMasterException {
		return publicationFacade.getPageablePublications(userGroup, page);
    }

	@PostMapping(value="/publications/request/{requestId}/checked/{requestType}", produces=MediaType.APPLICATION_JSON_VALUE)
    public PublicationDto checkedRequest(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long requestId, @PathVariable final Long requestType) throws SgMasterException {
		return publicationFacade.checkRequest(userGroup, requestId, requestType);
    }

	@PostMapping(value="/publications/request/{requestId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public PublicationDto addRequestType(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long requestId, @RequestBody final String requestType) throws SgMasterException {
		return publicationFacade.addRequestType(userGroup, requestId, requestType);
    }

	@PostMapping(value="/publications/post", produces=MediaType.APPLICATION_JSON_VALUE)
    public PublicationDto createPost(@AuthUser final UserGroupInformation userGroup, @RequestBody final CreatePostJson post) throws SgMasterException {
		return this.publicationFacade.createPost(userGroup, post);
    }

	@PutMapping(value="/publications/post/{postId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public PublicationDto updatePost(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long postId, @RequestBody final CreatePostJson post) throws SgMasterException {
		return this.publicationFacade.updatePost(userGroup, postId, post);
    }

	@DeleteMapping(value="/publications/request/{requestId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public void deleteRequest(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long requestId) throws SgMasterException {
		this.publicationFacade.deleteRequest(userGroup, requestId);
    }

	@PutMapping(value="/publications/post/{postId}/comment", produces=MediaType.APPLICATION_JSON_VALUE)
    public PublicationDto updateCommentToPost(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long postId, @RequestBody final CommentDto comment) throws SgMasterException {
		return publicationFacade.updateCommentToPost(userGroup, postId, comment);
    }

}
