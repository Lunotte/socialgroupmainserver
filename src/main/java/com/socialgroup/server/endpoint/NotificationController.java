package com.socialgroup.server.endpoint;

import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.NotificationDto;
import com.socialgroup.server.facade.NotificationFacade;

@RestController
@RequestMapping("/notification")
public class NotificationController {

	private final NotificationFacade notificationFacade;

	public NotificationController(final NotificationFacade notificationFacade) {
		this.notificationFacade = notificationFacade;
	}

	@PutMapping(value="/{notificationId}", produces=MediaType.APPLICATION_JSON_VALUE)
    public NotificationDto updateNotification(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long notificationId) throws SgMasterException {
		return this.notificationFacade.seenNotification(userGroup, notificationId);
    }

	@GetMapping(value="", produces=MediaType.APPLICATION_JSON_VALUE)
    public Page<NotificationDto> getNotifications(@AuthUser final UserGroupInformation userGroup, @RequestParam(value = "page", required = true) final int page) throws SgMasterException {
		return this.notificationFacade.notifications(userGroup, page);
    }
}
