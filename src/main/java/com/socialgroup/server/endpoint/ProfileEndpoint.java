package com.socialgroup.server.endpoint;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.common.exception.SgMasterException;
import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.MemberDto;
import com.socialgroup.server.endpoint.dto.ProfilDto;
import com.socialgroup.server.endpoint.dto.UpdatePersonalInformationJson;
import com.socialgroup.server.endpoint.dto.UserDto;
import com.socialgroup.server.facade.ProfileFacade;

@RestController
public class ProfileEndpoint {

	private final ProfileFacade profileFacade;

	public ProfileEndpoint(final ProfileFacade profileFacade) {
		this.profileFacade = profileFacade;
	}

	@GetMapping(value = "/profils", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProfilDto profil(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {
		return profileFacade.profil(userGroup);
	}

	@GetMapping(value = "/profils/users", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MemberDto> getUsersInCurrentGroup(@AuthUser final UserGroupInformation userGroup) throws SgMasterException {
		return profileFacade.getUsersInCurrentGroup(userGroup);
	}

	@PatchMapping(value = "/profils", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProfilDto updatePersonalInformation(@AuthUser final UserGroupInformation userGroup, @RequestBody final UpdatePersonalInformationJson updateInformation) throws SgMasterException {
		return profileFacade.updatePersonalInformation(userGroup, updateInformation);
	}

	@GetMapping(value = "/profils/user-registered", produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean profil(@RequestParam final String email) throws SgMasterException {
		return profileFacade.isRegisteredUser(email);
	}

	@PostMapping(value = "/profils/user", produces = MediaType.APPLICATION_JSON_VALUE)
	public UserDto createUser(@AuthUser final UserGroupInformation userGroup, @RequestBody final CreateUserJson createUser) throws SgMasterException {
		profileFacade.createUser(userGroup, createUser);
		return null;
	}

}
