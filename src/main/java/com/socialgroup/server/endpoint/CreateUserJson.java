package com.socialgroup.server.endpoint;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.socialgroup.server.facade.ICreateUser;


public class CreateUserJson implements ICreateUser{

	private final String email;

	private final LocalDate born;

	private final String firstname;

	private final String lastname;

	private final String username;

	private final Set<Long> groups = new HashSet<>(); // List id groups où l'on va ajouter le user - DOIT AU MOINS CONTENIR LE GROUP ACTIF

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public LocalDate getBorn() {
		return born;
	}

	@Override
	public String getFirstname() {
		return firstname;
	}

	@Override
	public String getLastname() {
		return lastname;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public Set<Long> getGroups() {
		return groups;
	}

	public CreateUserJson(final String email, final LocalDate born, final String firstname,
			final String lastname, final String username) {
		super();
		this.email = email;
		this.born = born;
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
	}

}