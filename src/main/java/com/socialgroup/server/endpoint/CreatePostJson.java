package com.socialgroup.server.endpoint;

import java.time.ZonedDateTime;
import java.util.List;

public class CreatePostJson {

	private ZonedDateTime start;
	
	private ZonedDateTime end;
	
	private String message;
	
	private List<String> requestsType;
	
	private boolean request;
	
	private boolean survey;

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(ZonedDateTime start) {
		this.start = start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getRequestsType() {
		return requestsType;
	}

	public void setRequestsType(List<String> requestsType) {
		this.requestsType = requestsType;
	}

	public boolean isRequest() {
		return request;
	}

	public void setRequest(boolean request) {
		this.request = request;
	}

	public boolean isSurvey() {
		return survey;
	}

	public void setSurvey(boolean survey) {
		this.survey = survey;
	}
	
}
