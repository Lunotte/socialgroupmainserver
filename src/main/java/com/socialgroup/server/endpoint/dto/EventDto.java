package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

import com.socialgroup.server.entity.Event;

public class EventDto {

	private Long id;

	private ZonedDateTime start;

	private ZonedDateTime end;

	// @JsonIgnore
	private PostDto post;

	private UserDto user;

	//@JsonIgnore
	private GroupDto group;

	private ZonedDateTime created;

	private ZonedDateTime updated;

	public EventDto() {
		super();
	}

	public EventDto(final Event event) {
		super();
		this.id = event.getId();
		this.start = event.getStart();
		this.end = event.getEnd();
		if (event.getPost() != null) {
			this.post = new PostDto(event.getPost());
		}
		this.user = new UserDto(event.getSource().getUser());
		this.group = new GroupDto(event.getSource().getGroup());
		this.created = event.getCreated();
		this.updated = event.getUpdated();
	}

	public EventDto(final Long id, final ZonedDateTime start, final ZonedDateTime end, final PostDto post, final UserDto user, final GroupDto group) {
		super();
		this.id = id;
		this.start = start;
		this.end = end;
		this.post = post;
		this.user = user;
		this.group = group;
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public ZonedDateTime getStart() {
		return start;
	}

	public void setStart(final ZonedDateTime start) {
		this.start = start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(final ZonedDateTime end) {
		this.end = end;
	}

	public PostDto getPost() {
		return post;
	}

	public void setPost(final PostDto post) {
		this.post = post;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(final UserDto user) {
		this.user = user;
	}

	public GroupDto getGroup() {
		return group;
	}

	public void setGroup(final GroupDto group) {
		this.group = group;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(final ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(final ZonedDateTime updated) {
		this.updated = updated;
	}

}
