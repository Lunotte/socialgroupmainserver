package com.socialgroup.server.endpoint.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import com.socialgroup.server.entity.User;

public class MemberDto extends PersonalInformationDto{

	private ZonedDateTime lastConnection;
	private int nbVote = 0;
	private boolean votedByMe = false;

	public MemberDto(final Long id, final String avatar, final String lastname, final String firstname,
			final String username, final LocalDate born, final ZonedDateTime lastConnection) {
		super(id, avatar, lastname, firstname, username, born);
		this.lastConnection = lastConnection;
	}

	public MemberDto(final User user) {
		super(user.getId(), user.getAvatar(), user.getLastname(), user.getFirstname(), user.getUsername(), user.getBorn());
		this.lastConnection = null;
	}

	/**
	 * @return the lastConnection
	 */
	public ZonedDateTime getLastConnection() {
		return lastConnection;
	}

	/**
	 * @param lastConnection the lastConnection to set
	 */
	public void setLastConnection(final ZonedDateTime lastConnection) {
		this.lastConnection = lastConnection;
	}

	/**
	 * @return the nbVote
	 */
	public int getNbVote() {
		return nbVote;
	}

	/**
	 * @param nbVote the nbVote to set
	 */
	public void setNbVote(final int nbVote) {
		this.nbVote = nbVote;
	}

	/**
	 * @return the votedByMe
	 */
	public boolean isVotedByMe() {
		return votedByMe;
	}

	/**
	 * @param votedByMe the votedByMe to set
	 */
	public void setVotedByMe(final boolean votedByMe) {
		this.votedByMe = votedByMe;
	}

}
