package com.socialgroup.server.endpoint.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.socialgroup.server.entity.User;
import com.socialgroup.server.entity.UserGroup;
import com.socialgroup.server.entity.literals.UserRole;

public class UserDto {

	private Long id;

	private ZonedDateTime created;

	private ZonedDateTime updated;

	private String email;

	private LocalDate born;

	private String avatar;

	private String firstname;

	private String lastname;

	private String username;

	@JsonIgnore
	private String password;

	@JsonIgnore
	private Set<GroupDto> groups = new HashSet<>();

	@JsonIgnore
	private Set<UserRole> roles;

	@JsonIgnore
	private Set<EventDto> events = new HashSet<>();

	@JsonIgnore
	private Set<FeedDto> feeds = new HashSet<>();

	@JsonIgnore
	private GroupDto group;

	public UserDto() {
		super();
	}

	public UserDto(final User user) {
		super();
		this.id = user.getId();
		this.email = user.getEmail();
		this.avatar = user.getAvatar();
		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.born = user.getBorn();
		for (final UserGroup g : user.getUserGroups()) {
			this.groups.add(new GroupDto(g));
		}
		this.roles = user.getRoles();
		this.group = new GroupDto(user.getGroup());
		this.created = user.getCreated();
		this.updated = user.getUpdated();
	}

	public UserDto(final Long id, final String email, final String avatar, final String firstname, final String lastname, final String username,
			final String password, final Set<GroupDto> groups, final Set<UserRole> roles, final Set<EventDto> events, final Set<FeedDto> feeds,
			final GroupDto group) {
		super();
		this.id = id;
		this.email = email;
		this.avatar = avatar;
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.groups = groups;
		this.roles = roles;
		this.events = events;
		this.feeds = feeds;
		this.group = group;
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(final String avatar) {
		this.avatar = avatar;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(final String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(final String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public Set<GroupDto> getGroups() {
		return groups;
	}

	public void setGroups(final Set<GroupDto> groups) {
		this.groups = groups;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(final Set<UserRole> roles) {
		this.roles = roles;
	}

	public Set<EventDto> getEvents() {
		return events;
	}

	public void setEvents(final Set<EventDto> events) {
		this.events = events;
	}

	public GroupDto getGroup() {
		return group;
	}

	public void setGroup(final GroupDto group) {
		this.group = group;
	}

	public Set<FeedDto> getFeeds() {
		return feeds;
	}

	public void setFeeds(final Set<FeedDto> feeds) {
		this.feeds = feeds;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(final ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(final ZonedDateTime updated) {
		this.updated = updated;
	}

	public LocalDate getBorn() {
		return born;
	}

	public void setBorn(final LocalDate born) {
		this.born = born;
	}
}
