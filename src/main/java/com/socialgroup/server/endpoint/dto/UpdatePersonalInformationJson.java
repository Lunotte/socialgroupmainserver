package com.socialgroup.server.endpoint.dto;

import java.time.LocalDate;

import com.socialgroup.server.facade.IUpdatePersonalInformation;

public class UpdatePersonalInformationJson implements IUpdatePersonalInformation {

	private final String avatar;

	private final String email;

	private final LocalDate born;

	private final String firstname;

	private final String lastname;

	private final String username;

	private final String password;

	public UpdatePersonalInformationJson(final String avatar, final String email,
			final LocalDate born, final String firstname, final String lastname, final String username,
			final String password) {
		this.avatar = avatar;
		this.email = email;
		this.born = born;
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
	}

	public UpdatePersonalInformationJson() {
		this.avatar = null;
		this.email = null;
		this.born = null;
		this.firstname = null;
		this.lastname = null;
		this.username = null;
		this.password = null;
	}

	/**
	 * @return the avatar
	 */
	@Override
	public String getAvatar() {
		return avatar;
	}

	public String getEmail()
	{
		return email;
	}

	/**
	 * @return the born
	 */
	@Override
	public LocalDate getBorn() {
		return born;
	}

	/**
	 * @return the firstname
	 */
	@Override
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @return the lastname
	 */
	@Override
	public String getLastname() {
		return lastname;
	}

	/**
	 * @return the username
	 */
	@Override
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	@Override
	public String getPassword() {
		return password;
	}

}
