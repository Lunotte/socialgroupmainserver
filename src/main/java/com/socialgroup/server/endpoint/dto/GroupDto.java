package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;
import java.util.Optional;

import com.socialgroup.server.entity.Group;
import com.socialgroup.server.entity.UserGroup;

public class GroupDto {

	private Long id;

	private String name;

	private String picture;

	private boolean defaultGroup;

	private ZonedDateTime connected;

	public GroupDto() {
	}

	public GroupDto(final UserGroup group) {
		this.id = group.getGroup().getId();
		this.name = group.getGroup().getName();
		this.connected = group.getLastConnextion();
		this.picture = group.getGroup().getPicture();
		this.defaultGroup = group.getUser().getGroup().equals(group.getGroup()); // Si ce group est celui par default
	}

	public GroupDto(final Group group) {
		this.id = group.getId();
		this.name = group.getName();
		this.picture = group.getPicture();
		final Optional<UserGroup>groupTmp = group.getUserGroups().stream().filter(ug -> ug.getId().getGroupId().equals(group.getId())).findFirst();
		this.connected = groupTmp.isPresent() ? groupTmp.get().getLastConnextion() : ZonedDateTime.now();
		this.defaultGroup = group.getUserGroups().stream().anyMatch(ug -> ug.getUser().getGroup().equals(group)); // Si ce group est celui par default
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public ZonedDateTime getConnected() {
		return connected;
	}

	public void setConnected(final ZonedDateTime connected) {
		this.connected = connected;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(final String picture) {
		this.picture = picture;
	}

	public boolean isDefaultGroup() {
		return defaultGroup;
	}

}
