package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

import com.socialgroup.server.entity.Notification;
import com.socialgroup.server.entity.literals.SourceNotificationEnum;

public class NotificationDto {

	private final Long id;
	private final String message;
	private final boolean seen;
	private final ZonedDateTime created;
	private final SourceNotificationEnum sourceNotification;

	public NotificationDto(final Notification notification) {
		this.id = notification.getId();
		this.message = notification.getMessage();
		this.seen = notification.isSeen();
		this.created = notification.getCreated();
		this.sourceNotification = notification.getSourceNotification();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the seen
	 */
	public boolean isSeen() {
		return seen;
	}

	/**
	 * @return the sourceNotification
	 */
	public SourceNotificationEnum getSourceNotification() {
		return sourceNotification;
	}

	/**
	 * @return the created
	 */
	public ZonedDateTime getCreated() {
		return created;
	}

}
