package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

import com.socialgroup.server.entity.CommentSurvey;
import com.socialgroup.server.entity.RequestType;
import com.socialgroup.server.entity.User;

public class CommentSurveyDto{

	private Long id;
	
	private ZonedDateTime created;
	
	private ZonedDateTime updated;

	//private Set<RequestTypeDto> requestTypes = new HashSet<>();
	private RequestTypeDto requestType;
	
	private UserDto user;
	
	private Long nbCommented;
	
	public CommentSurveyDto(CommentSurvey commentSurvey, RequestType requestType, User user, Long nbCommented) {
		this.id = commentSurvey.getId();
		this.created = commentSurvey.getCreated();
		this.updated = commentSurvey.getUpdated();
		this.requestType = new RequestTypeDto(requestType);
		this.nbCommented = nbCommented;
		this.user = new UserDto(user);
		/*for (CommentSurveyRequestType requestType : commentSurvey.getRequestType()) {
			this.requestTypes.add(new RequestTypeDto(requestType.getRequestType()));
		}*/
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getNbCommented() {
		return nbCommented;
	}

	public void setNbCommented(Long nbCommented) {
		this.nbCommented = nbCommented;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

	public RequestTypeDto getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestTypeDto requestType) {
		this.requestType = requestType;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

//	public Set<RequestTypeDto> getRequestTypes() {
//		return requestTypes;
//	}
//
//	public void setRequestType(Set<RequestTypeDto> requestTypes) {
//		this.requestTypes = requestTypes;
//	}
	
}
