package com.socialgroup.server.endpoint.dto;

import java.util.HashSet;
import java.util.Set;

import com.socialgroup.server.entity.CommentSurvey;
import com.socialgroup.server.entity.Request;
import com.socialgroup.server.entity.RequestType;

public class RequestDto extends EventDto {

	private String description;
	
	private boolean survey = false;

	private Set<RequestTypeDto> types = new HashSet<>();
	
	private Set<CommentSurveyDto> surveys = new HashSet<>();

	public RequestDto() {
		super();
	}

	public RequestDto(Request request) {
		super(request);
		this.description = request.getDescription();
		this.survey = (request.getStart() == null) ? false : true;
		for (RequestType requestType : request.getRequestTypes()) {
			this.types.add(new RequestTypeDto(requestType));
		}
		
		for (CommentSurvey commentSurvey : request.getCommentSurvey()) {
			this.surveys.add(new CommentSurveyDto(commentSurvey, commentSurvey.getRequestType(),
					commentSurvey.getUser(), request.getCommentSurvey().stream().filter(c -> c.getRequestType().getId().equals(commentSurvey.getRequestType().getId())).count()));
		}
	}

	public RequestDto(String description, boolean survey, Set<RequestType> requestTypes) {
		super();
		this.description = description;
		this.survey = survey;
		for (RequestType requestType : requestTypes) {
			this.types.add(new RequestTypeDto(requestType));
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<RequestTypeDto> getTypes() {
		return types;
	}

	public void setTypes(Set<RequestTypeDto> types) {
		this.types = types;
	}

	public boolean isSurvey() {
		return survey;
	}

	public void setSurvey(boolean survey) {
		this.survey = survey;
	}

	public Set<CommentSurveyDto> getSurveys() {
		return surveys;
	}

	public void setSurveys(Set<CommentSurveyDto> surveys) {
		this.surveys = surveys;
	}

}
