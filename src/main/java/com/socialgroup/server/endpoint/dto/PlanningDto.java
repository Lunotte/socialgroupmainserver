package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

public class PlanningDto {
	
	private Long id;
	
	private TypePlanningEnum typePlanning;
	
	private ZonedDateTime created;
	
	private ZonedDateTime updated;
	
	private RequestDto request;
	
	private AvailabilityDto availability;

	public PlanningDto(Long id, TypePlanningEnum typePlanning, ZonedDateTime created, ZonedDateTime updated,
			RequestDto request, AvailabilityDto availability) {
		super();
		this.id = id;
		this.typePlanning = typePlanning;
		this.created = created;
		this.updated = updated;
		this.request = request;
		this.availability = availability;
	}
	
	public PlanningDto(AvailabilityDto availabilityDto) {
		super();
		this.id = availabilityDto.getId();
		this.typePlanning = TypePlanningEnum.AVAILABILITY;
		this.created = availabilityDto.getCreated();
		this.updated = availabilityDto.getUpdated();
		this.availability = availabilityDto;
	}
	
	public PlanningDto(RequestDto requestDto) {
		super();
		this.id = requestDto.getId();
		this.typePlanning = TypePlanningEnum.EVENT;
		this.created = requestDto.getCreated();
		this.updated = requestDto.getUpdated();
		this.request = requestDto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypePlanningEnum getTypePlanning() {
		return typePlanning;
	}

	public void setTypePlanning(TypePlanningEnum typePlanning) {
		this.typePlanning = typePlanning;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

	public RequestDto getRequest() {
		return request;
	}

	public void setRequest(RequestDto request) {
		this.request = request;
	}

	public AvailabilityDto getAvailability() {
		return availability;
	}

	public void setAvailability(AvailabilityDto availability) {
		this.availability = availability;
	}
	
}
