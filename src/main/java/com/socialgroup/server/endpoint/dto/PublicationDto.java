package com.socialgroup.server.endpoint.dto;

import java.time.ZonedDateTime;

public class PublicationDto{
	
	private Long id;
	
	private TypeLineEnum typeLine;
	
	private ZonedDateTime created;
	
	private ZonedDateTime updated;
	
	private PostDto post;
	
	private RequestDto request;

	public PublicationDto(Long id, TypeLineEnum typeLine, ZonedDateTime created, ZonedDateTime updated,
			PostDto post, RequestDto request) {
		super();
		this.id = id;
		this.typeLine = typeLine;
		this.created = created;
		this.updated = updated;
		this.post = post;
		this.request = request;
	}

	public PublicationDto(PostDto post) {
		super();
		this.id = post.getId();
		this.typeLine = TypeLineEnum.Post;
		this.created = post.getCreated();
		this.updated = post.getUpdated();
		this.post = post;
	}
	
	public PublicationDto(RequestDto request) {
		super();
		this.id = request.getId();
		this.typeLine = TypeLineEnum.Request;
		this.created = request.getCreated();
		this.updated = request.getUpdated();
		this.request = request;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeLineEnum getTypeLine() {
		return typeLine;
	}

	public void setTypeLine(TypeLineEnum typeLine) {
		this.typeLine = typeLine;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

	public PostDto getPost() {
		return post;
	}

	public void setPost(PostDto post) {
		this.post = post;
	}

	public RequestDto getRequest() {
		return request;
	}

	public void setRequest(RequestDto request) {
		this.request = request;
	}
	
}
