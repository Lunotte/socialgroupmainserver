package com.socialgroup.server.endpoint.dto;

import com.socialgroup.server.entity.Availability;
import com.socialgroup.server.entity.Event;
import com.socialgroup.server.entity.literals.AvailabilityType;

public class AvailabilityDto extends EventDto {

	private AvailabilityType type;

	public AvailabilityDto() {
		super();
	}

	public AvailabilityDto(Availability availability) {
		super(availability);
		this.type = availability.getAvailabilityType();
	}

	public AvailabilityDto(Event event) {
		super(event);
	}

	public AvailabilityType getType() {
		return type;
	}

	public void setType(AvailabilityType type) {
		this.type = type;
	}


}
