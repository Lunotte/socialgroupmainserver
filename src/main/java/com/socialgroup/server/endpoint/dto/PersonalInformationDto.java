package com.socialgroup.server.endpoint.dto;

import java.time.LocalDate;

class PersonalInformationDto {

	private Long id;
	private String avatar;
	private String lastname;
	private String firstname;
	private String username;
	private LocalDate born;

	public PersonalInformationDto(final Long id, final String avatar, final String lastname,
			final String firstname, final String username, final LocalDate born) {
		this.id = id;
		this.avatar = avatar;
		this.lastname = lastname;
		this.firstname = firstname;
		this.username = username;
		this.born = born;
	}
	
	public Long getId() {
		return id;
	}

	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}
	/**
	 * @param avatar the avatar to set
	 */
	public void setAvatar(final String avatar) {
		this.avatar = avatar;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(final String lastname) {
		this.lastname = lastname;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(final String firstname) {
		this.firstname = firstname;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(final String username) {
		this.username = username;
	}
	/**
	 * @return the born
	 */
	public LocalDate getBorn() {
		return born;
	}
	/**
	 * @param born the born to set
	 */
	public void setBorn(final LocalDate born) {
		this.born = born;
	}

}
