package com.socialgroup.server.endpoint.dto;

import java.util.HashSet;
import java.util.Set;

import com.socialgroup.server.entity.Comment;
import com.socialgroup.server.entity.Post;

public class PostDto extends FeedDto {

	private Set<CommentDto> comments = new HashSet<>();

	private GroupDto group;

	public PostDto() {
		super();
	}

	public PostDto(Post post) {
		super(post);
		for (Comment comment : post.getComments()) {
			if(!comment.isSubComment()) {
				this.comments.add(new CommentDto(comment));
			}
		}
		this.group = new GroupDto(post.getGroup());
	}

	public PostDto(Set<CommentDto> comments, EventDto event, GroupDto group) {
		super();
		this.comments = comments;
		this.group = group;
	}

	public Set<CommentDto> getComments() {
		return comments;
	}

	public void setComments(Set<CommentDto> comments) {
		this.comments = comments;
	}

	public GroupDto getGroup() {
		return group;
	}

	public void setGroup(GroupDto group) {
		this.group = group;
	}

}
