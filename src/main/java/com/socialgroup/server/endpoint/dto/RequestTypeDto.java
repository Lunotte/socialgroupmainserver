package com.socialgroup.server.endpoint.dto;

import com.socialgroup.server.entity.RequestType;

public class RequestTypeDto {

	private Long id;
	private String name;
	
	public RequestTypeDto(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public RequestTypeDto(RequestType requestType) {
		this.setId(requestType.getId());
		this.setName(requestType.getName());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
