package com.socialgroup.server.endpoint.dto;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

import com.socialgroup.server.entity.User;

public class ProfilDto extends PersonalInformationDto{

	private final Collection<GroupDto> groups;

	public ProfilDto(final Long id, final String avatar, final String lastname, final String firstname,
			final String username, final LocalDate born, final Collection<GroupDto> groups) {
		super(id, avatar, lastname, firstname, username, born);
		this.groups = groups;
	}

	public ProfilDto(final User user) {
		
		super(user.getId(), user.getAvatar(), user.getLastname(), user.getFirstname(), user.getUsername(), user.getBorn());
		
		this.groups =  user.getUserGroups().stream()
				.map(g -> new GroupDto(g.getGroup())).collect(Collectors.toList());
	}

	/**
	 * @return the groups
	 */
	public Collection<GroupDto> getGroups() {
		return groups;
	}

}
