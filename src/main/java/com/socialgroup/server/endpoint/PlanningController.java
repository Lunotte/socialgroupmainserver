package com.socialgroup.server.endpoint;

import java.util.Collection;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialgroup.server.common.resolver.AuthUser;
import com.socialgroup.server.common.resolver.UserGroupInformation;
import com.socialgroup.server.endpoint.dto.AvailabilityDto;
import com.socialgroup.server.endpoint.dto.CreateAvailability;
import com.socialgroup.server.endpoint.dto.PlanningDto;
import com.socialgroup.server.facade.PlanningFacade;

@RestController
public class PlanningController {

	private final PlanningFacade planningFacade;

	public PlanningController(final PlanningFacade planningFacade) {
		this.planningFacade = planningFacade;
	}

	@GetMapping(value="/plannings", produces=MediaType.APPLICATION_JSON_VALUE)
    public Collection<PlanningDto> plannings(@AuthUser final UserGroupInformation userGroup) throws Exception {
		return this.planningFacade.getAllPlannings(userGroup);
    }

	@PostMapping(value="/plannings/availability", produces=MediaType.APPLICATION_JSON_VALUE)
    public AvailabilityDto createAvailability(@AuthUser final UserGroupInformation userGroup, @RequestBody final CreateAvailability availability) throws Exception {
		return this.planningFacade.postAvailability(userGroup, availability);
    }

	@PutMapping(value="/plannings/availability/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
    public AvailabilityDto updateAvailability(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long id, @RequestBody final CreateAvailability availability) throws Exception {
		return this.planningFacade.updateAvailability(userGroup, id, availability);
    }

	@DeleteMapping(value = "/plannings/availability/{eventId}")
	public void deleteAvailability(@AuthUser final UserGroupInformation userGroup, @PathVariable final Long eventId) throws Exception {
		this.planningFacade.deleteAvailability(userGroup, eventId);
	}

}
