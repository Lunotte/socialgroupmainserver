package com.socialgroup.server.cron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.socialgroup.server.facade.GroupFacade;
import com.socialgroup.server.facade.NotificationFacade;

@Component
public class CronTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(CronTask.class);

	private final GroupFacade groupFacade;
	private final NotificationFacade notificationFacade;

	public CronTask(final GroupFacade groupFacade, final NotificationFacade notificationFacade) {
		this.groupFacade = groupFacade;
		this.notificationFacade = notificationFacade;
	}

//	@Scheduled(fixedDelay = 10000)
//	public void scheduleFixedDelayTask() throws SgMasterException {
//		LOGGER.info("Task cron called for black list");
//		try {
//			this.groupFacade.checkBlackList();
//		} catch (final SgMasterException e) {
//			LOGGER.debug("An error has occured in task cron", e);
//		}
//	}

//	@Scheduled(fixedDelay = 10000)
//	public void scheduleFixedDelayTask() throws SgMasterException {
//		LOGGER.info("Task cron called");
//		try {
//			this.notificationFacade.deleteNotificationSeen();
//		} catch (final SgMasterException e) {
//			LOGGER.debug("An error has occured in task cron", e);
//		}
//	}

}
