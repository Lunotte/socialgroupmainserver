-- GROUP
INSERT INTO public.sg_groups(id, name) ( SELECT nextval('hibernate_sequence') , 'Groupe de test 1');
INSERT INTO public.sg_groups(id, name) ( SELECT nextval('hibernate_sequence') , 'Groupe de test 2');

-- USER
INSERT INTO public.sg_users(id, born, email, firstname, lastname, password, username, group_default_id, created, updated)
(SELECT nextval('hibernate_sequence'), '1991-06-10 00:00:00', 'charlybeaugrand@gmail.com', 'Charly', 'Beaugrand', '$2a$10$bnC26zz//2cavYoSCrlHdecWF8tkGfPodlHcYwlACBBwJvcEf0p2G', 'charly12', 1, NOW(), NOW());
INSERT INTO public.sg_users(id, born, email, firstname, lastname, password, username, group_default_id, created, updated)
(SELECT nextval('hibernate_sequence'), '1991-06-10 00:00:00', 'x-charlybg-x@hotmail.fr', 'Test', 'Test', '$2a$10$ZAZdOU8aHdd6mRKyUjVRLu7bIUT2uCe5vB8kKwiODRFWf/J/AS4Yu', 'test1', 1, NOW(), NOW());

-- USER ROLES
INSERT INTO public.sg_user_roles(role, app_user_id) VALUES ('MEMBER', 3);
INSERT INTO public.sg_user_roles(role, app_user_id) VALUES ('MEMBER', 4);

-- USER GROUPS
INSERT INTO public.sg_user_groups(group_id, user_id, connected, black_listed) VALUES (1, 3, NOW(), 'NO');
INSERT INTO public.sg_user_groups(group_id, user_id, connected, black_listed) VALUES (2, 4, NOW(), 'NO');